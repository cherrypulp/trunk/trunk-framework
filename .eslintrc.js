// @see https://eslint.org/docs/2.0.0/rules/
module.exports = {
    root: true,
    env: {
        browser: true,
        es2017: true,
        node: true,
        serviceworker: true,
        worker: true,
    },
    extends: ['plugin:prettier/recommended'],
    rules: {
        'constructor-super': 2,
        curly: 2,
        'dot-notation': 2,
        'eol-last': 2,
        eqeqeq: [2, 'smart'],
        'generator-star-spacing': [2, { before: false, after: true }],
        indent: ['error', 4, { SwitchCase: 1 }],
        //'key-spacing': [2, { align: 'value' }],
        'new-cap': 2,
        'newline-per-chained-call': 2,
        'new-parens': 2,
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-const-assign': 2,
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-dupe-class-members': 2,
        'no-else-return': 2,
        'no-eq-null': 2,
        'no-implicit-globals': 2,
        'no-negated-condition': 2,
        'no-new-object': 2,
        'no-new-symbol': 2,
        'no-spaced-func': 2,
        'no-this-before-super': 2,
        'no-var': 2,
        'no-whitespace-before-property': 2,
        'object-shorthand': 2,
        'prefer-const': 2,
        'prefer-template': 2,
        'prettier/prettier': ['error', { endOfLine: 'auto' }],
        'require-jsdoc':
            process.env.NODE_ENV === 'production'
                ? [
                      2,
                      {
                          require: {
                              FunctionDeclaration: true,
                              MethodDefinition: true,
                              ClassDeclaration: false,
                          },
                      },
                  ]
                : 'off',
        'space-before-blocks': [2, 'always'],
        'template-curly-spacing': [2, 'never'],
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
    },
    overrides: [
        {
            files: ['**/tests/*.js'],
            env: {
                mocha: true,
            },
        },
    ],
};
