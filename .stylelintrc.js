// @see https://stylelint.io/user-guide/rules
// @see https://github.com/kristerkari/stylelint-scss
module.exports = {
    extends: ['stylelint-config-standard', 'stylelint-prettier/recommended'],
    plugins: ['stylelint-scss'],
    rules: {
        'at-rule-no-unknown': null,
        'comment-whitespace-inside': ['always', { ignore: ['comments'] }],
        'declaration-empty-line-before': 'never',
        indentation: 4,
        // 'max-depth': [2, 10],
        'max-empty-lines': [2, { ignore: ['comments'] }],
        // 'max-params': [2, 4],
        // 'max-nested-callbacks': [2, 3],
        'no-descending-specificity': null,
        'prettier/prettier': [
            null,
            { endOfLine: 'auto', singleQuote: true, tabWidth: 4 },
        ],
        'selector-pseudo-class-no-unknown': [
            true,
            {
                ignorePseudoClasses: ['global'],
            },
        ],
        'selector-type-no-unknown': null,
        'unit-whitelist': ['deg', 'em', 'rem', '%', 'px'],
    },
};
