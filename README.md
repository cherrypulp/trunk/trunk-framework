# Trunk

> Frontend framework for cool kids



## Installation

Make sure all dependencies have been installed before moving on:

- [Node.js](http://nodejs.org/) >= 8.0.0
- [Laravel Mix](https://github.com/JeffreyWay/laravel-mix/) >= 5.0.0

```console
npm install --save @cherrypulp/trunk
```



### Webpack

```javascript
// webpack.mix.js
...
mix.webpackConfig({
    resolve: {
        alias: {
            # required for autoload
            '@': path.resolve(__dirname, paths.src),
        },
    },
});
...
```



## Quick start

### Configuration and Setup

Take a look to `config/` directory. You can setup a lot of things there.

```yaml
# mandatory
# TODO
```

```javascript
import { Application } from '@cherrypulp/trunk';

const app = new Application();

// Bind Dependencies
app.bind('Http', axios);

// Register Service Providers
app.register(new MyAwesomeServiceProvider(app));

document.addEventListener('DOMContentLoaded', () => app.boot());
```



### Structure

```bash
assets/
|--- js/
|    |--- config/                           # autoloaded by ConfigServiceProvider
|    |    |--- app.js
|    |--- main.js
|--- scss/
|    |--- config/                           # autoloaded by @cherrypulp/trunk/src/scss/index.scss
|    |    |--- index.scss
```



## Documentation

### Container

#### Binding dependencies

```javascript
app.bind('Http', axios);
```



```javascript
# TODO
```



## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkey_monk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/libraries/trunk-framework/blob/master/LICENSE) file for details.


