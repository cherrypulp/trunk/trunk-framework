"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _Container2 = _interopRequireDefault(require("./Container"));

var _AppServiceProvider = _interopRequireDefault(require("./providers/AppServiceProvider"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var _booted = new WeakMap();

var _loadedProviders = new WeakMap();

var _serviceProviders = new WeakMap();

var _bootProvider = new WeakSet();

var _markAsRegistered = new WeakSet();

var _registerBaseBindings = new WeakSet();

var _registerBaseServiceProviders = new WeakSet();

var Application =
/*#__PURE__*/
function (_Container) {
  (0, _inherits2["default"])(Application, _Container);

  var _super = _createSuper(Application);

  (0, _createClass2["default"])(Application, null, [{
    key: "getInstance",

    /**
     * The current globally available container (if any).
     * @type {Container|null}
     */

    /**
     * Indicates if the application has booted.
     * @type {boolean}
     */

    /**
     * The names of the loaded service providers.
     * @type {Map<string, boolean>}
     */

    /**
     * All of the registered service providers.
     * @type {Map<string, ServiceProvider>}
     */

    /**
     * Set the globally available instance of the container.
     * @param {Application, Container, string} className
     * @return {Container}
     */
    value: function getInstance() {
      var className = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Application;
      return _Container2["default"].getInstance(className);
    }
    /**
     * Set the shared instance of the container.
     * @param {Container} container
     * @param {Application, Container, string} className
     * @return {Container}
     */

  }, {
    key: "setInstance",
    value: function setInstance(container) {
      var className = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Application;
      return _Container2["default"].setInstance(container, className);
    }
    /**
     * Application constructor.
     */

  }]);

  function Application() {
    var _this;

    (0, _classCallCheck2["default"])(this, Application);
    _this = _super.call(this);

    _registerBaseServiceProviders.add((0, _assertThisInitialized2["default"])(_this));

    _registerBaseBindings.add((0, _assertThisInitialized2["default"])(_this));

    _markAsRegistered.add((0, _assertThisInitialized2["default"])(_this));

    _bootProvider.add((0, _assertThisInitialized2["default"])(_this));

    _booted.set((0, _assertThisInitialized2["default"])(_this), {
      writable: true,
      value: false
    });

    _loadedProviders.set((0, _assertThisInitialized2["default"])(_this), {
      writable: true,
      value: new Map()
    });

    _serviceProviders.set((0, _assertThisInitialized2["default"])(_this), {
      writable: true,
      value: new Map()
    });

    _classPrivateMethodGet((0, _assertThisInitialized2["default"])(_this), _registerBaseBindings, _registerBaseBindings2).call((0, _assertThisInitialized2["default"])(_this));

    _classPrivateMethodGet((0, _assertThisInitialized2["default"])(_this), _registerBaseServiceProviders, _registerBaseServiceProviders2).call((0, _assertThisInitialized2["default"])(_this));

    return _this;
  }
  /**
   * Boot the application's service providers.
   */


  (0, _createClass2["default"])(Application, [{
    key: "boot",
    value: function boot() {
      var _this2 = this;

      if ((0, _classPrivateFieldGet2["default"])(this, _booted)) {
        return;
      }

      (0, _classPrivateFieldGet2["default"])(this, _serviceProviders).forEach(function (provider) {
        _classPrivateMethodGet(_this2, _bootProvider, _bootProvider2).call(_this2, provider);
      });
      (0, _classPrivateFieldSet2["default"])(this, _booted, true);
    }
    /**
     * Boot the given service provider.
     * @param {ServiceProvider} provider
     * @return {*}
     */

  }, {
    key: "getProvider",

    /**
     * Get the registered service provider instance if it exists.
     * @param {string} provider
     * @return {ServiceProvider|null}
     */
    value: function getProvider(provider) {
      if ((0, _classPrivateFieldGet2["default"])(this, _serviceProviders).has(provider)) {
        return (0, _classPrivateFieldGet2["default"])(this, _serviceProviders).get(provider);
      }

      return null;
    }
    /**
     * Mark the given provider as registered.
     * @param {ServiceProvider} provider
     */

  }, {
    key: "register",

    /**
     * Register a service provider with the application.
     * @param {ServiceProvider|String} provider
     * @param {array} options
     * @param {boolean} force
     */
    value: function register(provider) {
      var _this3 = this;

      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      var force = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var registered = this.getProvider(provider);

      if (registered && !force) {
        return registered;
      }

      if (typeof provider === 'string') {
        provider = this.resolveProvider(provider);
      } // Set app instance if not already set


      if (!provider.app) {
        provider.app = this;
      }

      provider.registerProviders();

      if (typeof provider.bindings !== 'undefined') {
        Object.entries(provider.bindings).forEach(function (_ref) {
          var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
              key = _ref2[0],
              value = _ref2[1];

          _this3.bind(key, value);
        });
      }

      if (typeof provider.singletons !== 'undefined') {
        Object.entries(provider.singletons).forEach(function (_ref3) {
          var _ref4 = (0, _slicedToArray2["default"])(_ref3, 2),
              key = _ref4[0],
              value = _ref4[1];

          _this3.singleton(key, value);
        });
      }

      _classPrivateMethodGet(this, _markAsRegistered, _markAsRegistered2).call(this, provider);

      if ((0, _classPrivateFieldGet2["default"])(this, _booted)) {
        _classPrivateMethodGet(this, _bootProvider, _bootProvider2).call(this, provider);
      }

      return provider;
    }
    /**
     * Register base bindings.
     */

  }, {
    key: "resolveProvider",

    /**
     * Resolve a service provider instance from the classs.
     * @param {string} provider
     * @return {ServiceProvider|null}
     */
    value: function resolveProvider(provider) {
      if ((0, _classPrivateFieldGet2["default"])(this, _serviceProviders).has(provider)) {
        /* eslint-disable-next-line */
        provider = (0, _classPrivateFieldGet2["default"])(this, _serviceProviders).get(provider);
        return provider;
      }

      return null;
    }
    /**
     * Add a shortcut to the app instance.
     * @param dependencyName
     * @param customName
     * @param descriptor
     */

  }, {
    key: "shortcut",
    value: function shortcut(dependencyName) {
      var customName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      var descriptor = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

      if (!customName) {
        customName = dependencyName;
      }

      Object.defineProperty(this, customName, _objectSpread({
        configurable: true,
        enumerable: true,
        writable: false,
        value: this.use(dependencyName)
      }, descriptor));
    }
  }]);
  return Application;
}(_Container2["default"]);

exports["default"] = Application;
(0, _defineProperty2["default"])(Application, "instance", null);

var _bootProvider2 = function _bootProvider2(provider) {
  if (typeof provider.boot === 'function') {
    return provider.boot();
  }
};

var _markAsRegistered2 = function _markAsRegistered2(provider) {
  (0, _classPrivateFieldGet2["default"])(this, _serviceProviders).set(provider.constructor.name, provider);
  (0, _classPrivateFieldGet2["default"])(this, _loadedProviders).set(provider.constructor.name, true);
};

var _registerBaseBindings2 = function _registerBaseBindings2() {
  Application.setInstance(this);
  this.instance('app', this);
  this.instance('Container', this);
};

var _registerBaseServiceProviders2 = function _registerBaseServiceProviders2() {
  this.register(new _AppServiceProvider["default"](this));
};