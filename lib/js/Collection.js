"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _collect2 = _interopRequireDefault(require("collect.js"));

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

var Collection =
/*#__PURE__*/
function (_collect) {
  (0, _inherits2["default"])(Collection, _collect);

  var _super = _createSuper(Collection);

  /**
   * @param items
   */
  function Collection(items) {
    var _this;

    (0, _classCallCheck2["default"])(this, Collection);
    _this = _super.call(this, items);
    /**
     * Get value through dot notation path.
     * @param key
     * @param defaultValue
     * @return {*}
     */

    _this.getThrough = function (key) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      try {
        return key.split('.').reduce(function (acc, prop) {
          return acc[prop];
        }, _this.items) || defaultValue;
      } catch (err) {
        return defaultValue;
      }
    };
    /**
     * Check if a key exists through path.
     * @param keys
     * @return {boolean}
     */


    _this.hasThrough = function (keys) {
      if (!Array.isArray(keys)) {
        keys = keys.split(' ');
      }

      return keys.filter(function (key) {
        return _this.getThrough(key);
      }).length === keys.length;
    };
    /**
     * Set given value through dot notation path.
     * @param key
     * @param value
     * @return {*}
     */


    _this.setThrough = function (key, value) {
      var keys = key.split('.');
      var source = _this.items;

      for (var i = 0, len = keys.length; i < len; i++) {
        var k = keys[i];

        if (i === keys.length - 1) {
          source[k] = value;
        }

        if (source[k] === undefined) {
          source[k] = {};
        }

        source = source[k];
      }

      return (0, _assertThisInitialized2["default"])(_this);
    };

    return _this;
  }

  return Collection;
}(_collect2["default"]);

exports["default"] = Collection;