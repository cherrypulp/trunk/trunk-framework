"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _LoggerInterface2 = _interopRequireDefault(require("./LoggerInterface"));

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var colors = {
  black: '#000',
  blue: '#71bef2',
  cyan: '#66c2cd',
  green: '#a9cc8d',
  pink: '#d291e4',
  red: '#e88388',
  white: '#fff',
  yellow: '#dbab79'
};
var styles = {
  emergency: "background-color: ".concat(colors.ping, "; color: ").concat(colors.black),
  alert: "color: ".concat(colors.ping),
  critical: "background-color: ".concat(colors.red, "; color: ").concat(colors.black),
  error: "color: ".concat(colors.red),
  warning: "color: ".concat(colors.yellow),
  notice: "background-color: ".concat(colors.cyan, "; color: ").concat(colors.black),
  info: "color: ".concat(colors.cyan),
  debug: "color: ".concat(colors.blue),
  log: ''
};

var ConsoleLogger =
/*#__PURE__*/
function (_LoggerInterface) {
  (0, _inherits2["default"])(ConsoleLogger, _LoggerInterface);

  function ConsoleLogger() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2["default"])(this, ConsoleLogger);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2["default"])(this, (_getPrototypeOf2 = (0, _getPrototypeOf3["default"])(ConsoleLogger)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _writeLog.add((0, _assertThisInitialized2["default"])(_this));

    return _this;
  }

  (0, _createClass2["default"])(ConsoleLogger, [{
    key: "formatMessage",
    value: function formatMessage(message) {
      return message;
    }
  }, {
    key: "emergency",
    value: function emergency(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'emergency', message, context);
    }
  }, {
    key: "alert",
    value: function alert(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'alert', message, context);
    }
  }, {
    key: "critical",
    value: function critical(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'critical', message, context);
    }
  }, {
    key: "error",
    value: function error(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'error', message, context);
    }
  }, {
    key: "warning",
    value: function warning(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'warning', message, context);
    }
  }, {
    key: "notice",
    value: function notice(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'notice', message, context);
    }
  }, {
    key: "info",
    value: function info(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'info', message, context);
    }
  }, {
    key: "debug",
    value: function debug(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'debug', message, context);
    }
  }, {
    key: "log",
    value: function log(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'log', message, context);
    }
  }]);
  return ConsoleLogger;
}(_LoggerInterface2["default"]);

exports["default"] = ConsoleLogger;

var _writeLog = new WeakSet();

var _writeLog2 = function _writeLog2(level, message, context) {
  var format = '%c';

  if (typeof message === 'string') {
    format += '%s';
  } else {
    format += '%o';
  }

  console.log(format, styles[level], message);
};