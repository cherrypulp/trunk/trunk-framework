"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _Exception = _interopRequireDefault(require("./Exception"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _aliases = new WeakMap();

var _bindings = new WeakMap();

var _resolved = new WeakMap();

var Container =
/*#__PURE__*/
function () {
  function Container() {
    (0, _classCallCheck2["default"])(this, Container);

    _aliases.set(this, {
      writable: true,
      value: new Map()
    });

    _bindings.set(this, {
      writable: true,
      value: new Map()
    });

    _resolved.set(this, {
      writable: true,
      value: new Map()
    });
  }

  (0, _createClass2["default"])(Container, [{
    key: "alias",

    /**
     * Alias a type to a different name.
     * Keeping the alias unique is the responsibility of the user.
     * @example
     * ```js
     * ioc.alias('App/User', 'user');
     * ```
     * @param {string} namespace
     * @param {string} alias
     */
    value: function alias(namespace, _alias) {
      (0, _classPrivateFieldGet2["default"])(this, _aliases).set(_alias, namespace);
    }
    /**
     * Register a binding with the container.
     * Keeping the namespace unique is the responsibility of the user.
     * @example
     * ```js
     * ioc.bind('App/User', () => new User());
     * ```
     * @param {string} namespace
     * @param {Function|String|null} callback
     */

  }, {
    key: "bind",
    value: function bind(namespace) {
      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (typeof callback !== 'function') {
        var _callback = callback;

        callback = function callback() {
          return _callback;
        };
      }

      (0, _classPrivateFieldGet2["default"])(this, _bindings).set(namespace, {
        namespace: namespace,
        callback: callback,
        singleton: false
      });
    }
    /**
     * @param {string} namespace
     * @return {*}
     */

  }, {
    key: "get",
    value: function get(namespace) {
      return this.resolve(namespace);
    }
    /**
     * Get the alis for an abstract if available.
     * @param {string} namespace
     * @return {String}
     */

  }, {
    key: "getAlias",
    value: function getAlias(namespace) {
      if ((0, _classPrivateFieldGet2["default"])(this, _aliases).has(namespace)) {
        return (0, _classPrivateFieldGet2["default"])(this, _aliases).get(namespace);
      }

      return namespace;
    }
    /**
     * @example
     * ```js
     * ioc.has('App/User');     // namespace
     * ioc.has('user', true);   // alias
     * ```
     * @param {string} namespace
     * @param {boolean} checkAliases
     * @return {boolean}
     */

  }, {
    key: "has",
    value: function has(namespace) {
      var checkAliases = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      if (checkAliases) {
        return (0, _classPrivateFieldGet2["default"])(this, _bindings).has(this.getAlias(namespace));
      }

      return (0, _classPrivateFieldGet2["default"])(this, _bindings).has(namespace);
    }
    /**
     * Register an existing instance as shared in the container.
     * @example
     * ```js
     * ioc.instance('app', this);
     * ```
     * @param namespace
     * @param instance
     */

  }, {
    key: "instance",
    value: function instance(namespace, _instance) {
      namespace = this.getAlias(namespace);
      var binding = {
        cachedValue: _instance,
        callback: null,
        namespace: namespace,
        singleton: true
      };

      if ((0, _classPrivateFieldGet2["default"])(this, _bindings).has(namespace)) {
        binding = _objectSpread(_objectSpread({}, (0, _classPrivateFieldGet2["default"])(this, _bindings).get(namespace)), {}, {
          cachedValue: _instance
        });
      }

      (0, _classPrivateFieldGet2["default"])(this, _bindings).set(namespace, binding);
    }
    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ioc.make('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.make('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.make('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */

  }, {
    key: "make",
    value: function make(namespace) {
      var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      return this.resolve(namespace, parameters);
    }
    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ioc.resolve('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.resolve('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.resolve('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */

  }, {
    key: "resolve",
    value: function resolve(namespace) {
      var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      namespace = this.getAlias(namespace);

      if (!this.has(namespace)) {
        throw new _Exception["default"]("Cannot resolve ".concat(namespace, " binding from the IoC Container"), 500, 'E_IOC_BINDING_NOT_FOUND');
      }

      var binding = (0, _classPrivateFieldGet2["default"])(this, _bindings).get(namespace);

      if (binding.singleton) {
        if (binding.cachedValue === undefined) {
          binding.cachedValue = parameters.length ? binding.callback.apply(binding, [this].concat((0, _toConsumableArray2["default"])(parameters))) : binding.callback(this);
        }

        (0, _classPrivateFieldGet2["default"])(this, _resolved).set(namespace, true);
        return binding.cachedValue;
      }

      (0, _classPrivateFieldGet2["default"])(this, _resolved).set(namespace, true);
      return parameters.length ? binding.callback.apply(binding, [this].concat((0, _toConsumableArray2["default"])(parameters))) : binding.callback(this);
    }
    /**
     * Register a shared binding in the container.
     * The callback will be invoked only once.
     * @example
     * ```js
     * ioc.singleton('App/User', () => new User());
     * ```
     * @param {string} namespace
     * @param {*} callback
     */

  }, {
    key: "singleton",
    value: function singleton(namespace) {
      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (typeof callback !== 'function') {
        var _callback = callback;

        callback = function callback() {
          return _callback;
        };
      }

      (0, _classPrivateFieldGet2["default"])(this, _bindings).set(namespace, {
        namespace: namespace,
        callback: callback,
        singleton: true
      });
    }
    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ios.use('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.use('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.use('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */

  }, {
    key: "use",
    value: function use(namespace) {
      var parameters = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      return this.resolve(namespace, parameters);
    }
    /**
     * Execute a callback by resolving bindings from the container.
     * Only executed when all bindings exists in the container.
     * @example
     * ```js
     * ioc.with(['App/User'], (User) => {
     *     // do something with User...
     * });
     * ```
     * @param {array} namespaces
     * @param {Function|null} callback
     */

  }, {
    key: "with",
    value: function _with(namespaces) {
      var _this = this;

      var callback = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (typeof namespaces === 'string') {
        namespaces = [namespaces];
      }

      if (namespaces.every(function (namespace) {
        return _this.has(namespace, true);
      })) {
        callback.apply(void 0, (0, _toConsumableArray2["default"])(namespaces.map(function (namespace) {
          return _this.use(namespace);
        })));
      }
    }
  }], [{
    key: "getInstance",

    /**
     * Set the globally available instance of the container.
     * @param {Container} className
     * @return {Container}
     */
    value: function getInstance() {
      var className = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Container;

      if (className.instance === null) {
        /* eslint-disable-next-line */
        className.instance = new className();
      }

      return className.instance;
    }
    /**
     * Set the shared instance of the container.
     * @param {Container} container
     * @param className
     * @return {Container}
     */

  }, {
    key: "setInstance",
    value: function setInstance(container) {
      var className = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Container;
      return className.instance = container;
    }
  }]);
  return Container;
}();

exports["default"] = Container;
(0, _defineProperty2["default"])(Container, "instance", null);