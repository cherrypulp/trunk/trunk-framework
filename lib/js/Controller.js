"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var _do = new WeakSet();

var _getName = new WeakSet();

var _trigger = new WeakSet();

/**
 * Controller class to use instead of @dogstudio/highway Rendered class.
 * @example
 * class MyController extends Controller {
 *     // do something...
 * }
 */
var Controller =
/*#__PURE__*/
function () {
  /**
   * Application container.
   * @type {Application|Container|null}
   */

  /**
   * Controller name.
   * @type {null}
   */

  /**
   * Controller constructor.
   * @param {Application|Container} container
   */
  function Controller(container) {
    (0, _classCallCheck2["default"])(this, Controller);

    _trigger.add(this);

    _getName.add(this);

    _do.add(this);

    (0, _defineProperty2["default"])(this, "app", null);
    (0, _defineProperty2["default"])(this, "name", null);
    this.app = container;
    this.doBeforeCreate();
  }
  /**
   * @return {*}
   */


  (0, _createClass2["default"])(Controller, [{
    key: "isActive",
    value: function isActive() {
      return document.body.className.toLowerCase().split(/\s+/).includes(_classPrivateMethodGet(this, _getName, _getName2).call(this));
    }
    /**
     * Executed when ControllerProvider instantiate controllers.
     */

  }, {
    key: "beforeCreate",
    value: function beforeCreate() {}
    /**
     * Executed on ServiceProvider register.
     */

  }, {
    key: "created",
    value: function created() {}
    /**
     * Do internal helper.
     * @param {string} type
     */

  }, {
    key: "doBeforeCreate",

    /**
     * Do beforeCreate callback and trigger an event.
     */
    value: function doBeforeCreate() {
      _classPrivateMethodGet(this, _do, _do2).call(this, 'beforeCreate');
    }
    /**
     * Do created callback and trigger an event.
     */

  }, {
    key: "doCreated",
    value: function doCreated() {
      _classPrivateMethodGet(this, _do, _do2).call(this, 'created');
    }
    /**
     * Do mounted callback and trigger an event.
     */

  }, {
    key: "doMounted",
    value: function doMounted() {
      _classPrivateMethodGet(this, _do, _do2).call(this, 'mounted');
    }
    /**
     * Get name value.
     * @return {*}
     */

  }, {
    key: "mounted",

    /**
     * Executed on ServiceProvider boot.
     */
    value: function mounted() {}
    /**
     * Trigger an event.
     * @param {string} eventName
     */

  }]);
  return Controller;
}();

exports["default"] = Controller;

var _do2 = function _do2(type) {
  this[type]();

  _classPrivateMethodGet(this, _trigger, _trigger2).call(this, type);
};

var _getName2 = function _getName2() {
  return this.name ? this.name : this.constructor.name.toLowerCase();
};

var _trigger2 = function _trigger2(eventName) {
  var events = this.app.get('events');
  events.dispatch(eventName, this);
  events.dispatch("".concat(this.constructor.name, ".").concat(eventName), this);
};