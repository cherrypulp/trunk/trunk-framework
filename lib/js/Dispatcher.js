"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _Container = _interopRequireDefault(require("./Container"));

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var _container = new WeakMap();

var _listeners = new WeakMap();

var _wildcards = new WeakMap();

var _wildcardsCache = new WeakMap();

var _getWildcardListeners = new WeakSet();

var _resolveSubscriber = new WeakSet();

var _setupWildcardListener = new WeakSet();

var Dispatcher =
/*#__PURE__*/
function () {
  /**
   * The IoC container instance.
   * @type {Container|null}
   */

  /**
   * The registered event listeners.
   * @type {Map<String, Array>}
   */

  /**
   * The wildcard listeners.
   * @type {Map<String, Array>}
   */

  /**
   * The cached wildcard listeners.
   * @type {Map<String, Array>}
   */

  /**
   * @param container
   */
  function Dispatcher(container) {
    (0, _classCallCheck2["default"])(this, Dispatcher);

    _setupWildcardListener.add(this);

    _resolveSubscriber.add(this);

    _getWildcardListeners.add(this);

    _container.set(this, {
      writable: true,
      value: null
    });

    _listeners.set(this, {
      writable: true,
      value: new Map()
    });

    _wildcards.set(this, {
      writable: true,
      value: new Map()
    });

    _wildcardsCache.set(this, {
      writable: true,
      value: new Map()
    });

    (0, _classPrivateFieldSet2["default"])(this, _container, container !== null && container !== void 0 ? container : new _Container["default"]());
  }
  /**
   * Fire an event and call the listeners.
   * @param event
   * @param payload
   * @param halt
   * @return {*}
   */


  (0, _createClass2["default"])(Dispatcher, [{
    key: "dispatch",
    value: function dispatch(event) {
      var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var halt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      var listeners = this.getListeners(event);
      var responses = [];
      var continues = true;
      listeners.forEach(function (listener) {
        if (continues) {
          var response = listener(event, payload);

          if (halt && response !== null) {
            return response;
          }

          if (response === false) {
            continues = false;
          } else {
            responses.push(response);
          }
        }
      });
      return halt ? null : responses;
    }
    /**
     * Fire an event and call the listeners.
     * @param {string} event
     * @param payload
     * @param {boolean} halt
     * @return {*}
     */

  }, {
    key: "fire",
    value: function fire(event) {
      var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var halt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      return this.dispatch(event, payload, halt);
    }
    /**
     * Flush a set of pushed events.
     * @param {string} event
     */

  }, {
    key: "flush",
    value: function flush(event) {
      this.dispatch("".concat(event, "_pushed"));
    }
    /**
     * Remove a set of listeners from the dispatcher.
     * @param {string} event
     */

  }, {
    key: "forget",
    value: function forget(event) {
      if (event.includes('*')) {
        (0, _classPrivateFieldGet2["default"])(this, _wildcards)["delete"](event);
      } else {
        (0, _classPrivateFieldGet2["default"])(this, _listeners)["delete"](event);
      }
    }
    /**
     * Forget all of the pushed listeners.
     */

  }, {
    key: "forgetPushed",
    value: function forgetPushed() {
      var _this = this;

      (0, _classPrivateFieldGet2["default"])(this, _listeners).forEach(function (listener, key) {
        if (key.endsWith('_pushed')) {
          _this.forget(key);
        }
      });
    }
    /**
     * Get all of the listeners for a given event name.
     * @param {string} eventName
     * @return {*[]}
     */

  }, {
    key: "getListeners",
    value: function getListeners(eventName) {
      var listeners = (0, _classPrivateFieldGet2["default"])(this, _listeners).get(eventName) || [];
      var wildcards = (0, _classPrivateFieldGet2["default"])(this, _wildcardsCache).has(eventName) ? (0, _classPrivateFieldGet2["default"])(this, _wildcardsCache).get(eventName) : _classPrivateMethodGet(this, _getWildcardListeners, _getWildcardListeners2).call(this, eventName);
      return [].concat(listeners, wildcards);
    }
    /**
     * Get the wildcard listeners for the event.
     * @param {string} eventName
     * @return {{}}
     */

  }, {
    key: "hasListeners",

    /**
     * Determine if a given event has listeners.
     * @param {string} eventName
     * @return {Boolean}
     */
    value: function hasListeners(eventName) {
      var listeners = (0, _classPrivateFieldGet2["default"])(this, _listeners).get(eventName) || [];
      var wildcards = (0, _classPrivateFieldGet2["default"])(this, _wildcards).get(eventName) || [];
      return listeners.length > 0 || wildcards.length > 0;
    }
    /**
     * Register an event listener with the dispatcher.
     * @param {String|Array} events
     * @param {Function} listener
     */

  }, {
    key: "listen",
    value: function listen(events, listener) {
      var _this2 = this;

      if (!Array.isArray(events)) {
        events = events.split(' ');
      }

      events.forEach(function (event) {
        if (event.includes('*')) {
          _classPrivateMethodGet(_this2, _setupWildcardListener, _setupWildcardListener2).call(_this2, event, listener);
        } else {
          var listeners = (0, _classPrivateFieldGet2["default"])(_this2, _listeners).get(event) || [];
          listeners.push(_this2.makeListener(listener));
          (0, _classPrivateFieldGet2["default"])(_this2, _listeners).set(event, listeners);
        }
      });
    }
    /**
     * Register an event listener with the dispatcher.
     * @param {Function} listener
     * @param {boolean} wildcard
     * @return {function(...[*]=)}
     */

  }, {
    key: "makeListener",
    value: function makeListener(listener) {
      var wildcard = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      return function (event, payload) {
        if (wildcard) {
          return listener(event, payload);
        }

        return listener(payload);
      };
    }
    /**
     * Register an event and payload to be fired later.
     * @param {string} event
     * @param payload
     */

  }, {
    key: "push",
    value: function push(event) {
      var _this3 = this;

      var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      this.listen("".concat(event, "_pushed"), function () {
        _this3.dispatch(event, payload);
      });
    }
    /**
     * Resolve the subscriber instance.
     * @param {Object|String} subscriber
     * @return {*}
     */

  }, {
    key: "subscribe",

    /**
     * Register an event subscriber with the dispatcher.
     * @param {Dispatcher|Object|String} subscriber
     */
    value: function subscribe(subscriber) {
      subscriber = _classPrivateMethodGet(this, _resolveSubscriber, _resolveSubscriber2).call(this, subscriber);
      subscriber.subscribe(this);
    }
    /**
     * Fire an event until the first non-null response is returned.
     * @param {string} event
     * @param payload
     * @return {*}
     */

  }, {
    key: "until",
    value: function until(event) {
      var payload = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      return this.dispatch(event, payload, true);
    }
  }]);
  return Dispatcher;
}();

exports["default"] = Dispatcher;

var _getWildcardListeners2 = function _getWildcardListeners2(eventName) {
  var wildcards = [];
  (0, _classPrivateFieldGet2["default"])(this, _wildcards).forEach(function (listeners, key) {
    if (key === eventName) {
      wildcards = [].concat(wildcards, listeners);
    }
  });
  (0, _classPrivateFieldGet2["default"])(this, _wildcardsCache).set(eventName, wildcards);
  return wildcards;
};

var _resolveSubscriber2 = function _resolveSubscriber2(subscriber) {
  if (typeof subscriber === 'string') {
    return (0, _classPrivateFieldGet2["default"])(this, _container).make(subscriber);
  }

  return subscriber;
};

var _setupWildcardListener2 = function _setupWildcardListener2(event, listener) {
  var wildcards = (0, _classPrivateFieldGet2["default"])(this, _wildcards).get(event) || [];
  wildcards.push(this.makeListener(listener, true));
  (0, _classPrivateFieldGet2["default"])(this, _wildcards).set(event, wildcards);
  (0, _classPrivateFieldSet2["default"])(this, _wildcardsCache, new Map());
};