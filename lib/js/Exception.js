"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _wrapNativeSuper2 = _interopRequireDefault(require("@babel/runtime/helpers/wrapNativeSuper"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

/**
 * Extended Error object with the option to set error `status` and `code`.
 *
 * @example
 * ```js
 * new Exception('message', 500, 'E_RUNTIME_EXCEPTION');
 * ```
 */
var Exception =
/*#__PURE__*/
function (_Error) {
  (0, _inherits2["default"])(Exception, _Error);

  var _super = _createSuper(Exception);

  /**
   * Exception code.
   * @type {String|null}
   */

  /**
   * Exception message.
   * @type {String}
   */

  /**
   * Exception name.
   * @type {String}
   */

  /**
   * Exception status number.
   * @type {Number}
   */

  /**
   * Exception constructor.
   * @param message
   * @param status
   * @param code
   */
  function Exception(message) {
    var _this;

    var status = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
    var code = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
    (0, _classCallCheck2["default"])(this, Exception);
    _this = _super.call(this, message); // Set error message

    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "code", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "message", 'Exception');
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "name", null);
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "status", null);
    Object.defineProperty((0, _assertThisInitialized2["default"])(_this), 'message', {
      configurable: true,
      enumerable: false,
      value: code ? "".concat(code, ": ").concat(message) : message,
      writable: true
    }); // Set error name as public property

    Object.defineProperty((0, _assertThisInitialized2["default"])(_this), 'name', {
      configurable: true,
      enumerable: false,
      value: _this.constructor.name,
      writable: true
    }); // Set status as a public property

    if (code) {
      Object.defineProperty((0, _assertThisInitialized2["default"])(_this), 'code', {
        configurable: true,
        enumerable: false,
        value: code,
        writable: true
      });
    } // Update stack trace


    Error.captureStackTrace((0, _assertThisInitialized2["default"])(_this), _this.constructor);
    return _this;
  }

  return Exception;
}(
/*#__PURE__*/
(0, _wrapNativeSuper2["default"])(Error));

exports["default"] = Exception;