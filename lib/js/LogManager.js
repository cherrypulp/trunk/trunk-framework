"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _Exception = _interopRequireDefault(require("./Exception"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var _activeChannels = new WeakMap();

var _container = new WeakMap();

var _channels = new WeakMap();

var _dispatcher = new WeakMap();

var _levels = new WeakMap();

var _fireLogEvent = new WeakSet();

var _resolve = new WeakSet();

var _writeLog = new WeakSet();

var LogManager =
/*#__PURE__*/
function () {
  (0, _createClass2["default"])(LogManager, [{
    key: "alert",

    /**
     * Active channel.
     * @type {[]}
     */

    /**
     * The IoC container instance.
     * @type {Container|null}
     */

    /**
     * The array of resolved channels.
     * @type {{}}
     */

    /**
     * The event dispatcher instance.
     * @type {Dispatcher|null}
     */

    /**
     * Log levels.
     * @type {}
     */

    /**
     * Log an alert message to the logs.
     * @param message
     * @param context
     */
    value: function alert(message) {
      for (var _len = arguments.length, context = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        context[_key - 1] = arguments[_key];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'alert', message, context);
    }
    /**
     * Get a log channel instance.
     * @example
     *     this.channel().log(something);
     * @example
     *     this.channel('console').log(something);
     * @example
     *     this.channel(['console', 'report']).log(something);
     * @param name
     * @return {*}
     */

  }, {
    key: "channel",
    value: function channel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'console';

      if (!Array.isArray(name)) {
        name = [name];
      }

      (0, _classPrivateFieldSet2["default"])(this, _activeChannels, name);
      return this;
    }
    /**
     * Create a new Log manager instance.
     * @param container
     */

  }]);

  function LogManager(container) {
    (0, _classCallCheck2["default"])(this, LogManager);

    _writeLog.add(this);

    _resolve.add(this);

    _fireLogEvent.add(this);

    _activeChannels.set(this, {
      writable: true,
      value: []
    });

    _container.set(this, {
      writable: true,
      value: null
    });

    _channels.set(this, {
      writable: true,
      value: {}
    });

    _dispatcher.set(this, {
      writable: true,
      value: null
    });

    _levels.set(this, {
      writable: true,
      value: {
        emergency: 600,
        alert: 550,
        critical: 500,
        error: 400,
        warning: 300,
        notice: 250,
        info: 200,
        debug: 100,
        log: 50,
        off: 0
      }
    });

    (0, _classPrivateFieldSet2["default"])(this, _container, container);

    if (!(0, _classPrivateFieldGet2["default"])(this, _container).has('events')) {
      throw new _Exception["default"]('Events dispatcher has not been set.', 500, 'E_RUNTIME_EXCEPTION');
    }

    (0, _classPrivateFieldSet2["default"])(this, _dispatcher, (0, _classPrivateFieldGet2["default"])(this, _container).get('events'));
  }
  /**
   * Log a critical message to the logs.
   * @param message
   * @param context
   */


  (0, _createClass2["default"])(LogManager, [{
    key: "critical",
    value: function critical(message) {
      for (var _len2 = arguments.length, context = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        context[_key2 - 1] = arguments[_key2];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'critical', message, context);
    }
    /**
     * Log a debug message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "debug",
    value: function debug(message) {
      for (var _len3 = arguments.length, context = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
        context[_key3 - 1] = arguments[_key3];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'debug', message, context);
    }
    /**
     * Log an emergency message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "emergency",
    value: function emergency(message) {
      for (var _len4 = arguments.length, context = new Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
        context[_key4 - 1] = arguments[_key4];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'emergency', message, context);
    }
    /**
     * Log an error message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "error",
    value: function error(message) {
      for (var _len5 = arguments.length, context = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
        context[_key5 - 1] = arguments[_key5];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'error', message, context);
    }
    /**
     * Fire a log event.
     * @param level
     * @param message
     * @param context
     */

  }, {
    key: "info",

    /**
     * Log an informational message to the logs.
     * @param message
     * @param context
     */
    value: function info(message) {
      for (var _len6 = arguments.length, context = new Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
        context[_key6 - 1] = arguments[_key6];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'info', message, context);
    }
    /**
     * Register a new callback handler for when a log event is triggered.
     * @param level
     * @param listener
     */

  }, {
    key: "listen",
    value: function listen(level, listener) {
      (0, _classPrivateFieldGet2["default"])(this, _dispatcher).listen(level, listener);
    }
    /**
     * Log a message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "log",
    value: function log(message) {
      for (var _len7 = arguments.length, context = new Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
        context[_key7 - 1] = arguments[_key7];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'log', message, context);
    }
    /**
     * Log a notice to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "notice",
    value: function notice(message) {
      for (var _len8 = arguments.length, context = new Array(_len8 > 1 ? _len8 - 1 : 0), _key8 = 1; _key8 < _len8; _key8++) {
        context[_key8 - 1] = arguments[_key8];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'notice', message, context);
    }
    /**
     * Resolve the given log instance by name.
     * @param name
     * @return {*}
     */

  }, {
    key: "warning",

    /**
     * Log a warning message to the logs.
     * @param message
     * @param context
     */
    value: function warning(message) {
      for (var _len9 = arguments.length, context = new Array(_len9 > 1 ? _len9 - 1 : 0), _key9 = 1; _key9 < _len9; _key9++) {
        context[_key9 - 1] = arguments[_key9];
      }

      _classPrivateMethodGet(this, _writeLog, _writeLog2).call(this, 'warning', message, context);
    }
    /**
     * Write a message to the log.
     * @param level
     * @param message
     * @param context
     */

  }]);
  return LogManager;
}();

exports["default"] = LogManager;

var _fireLogEvent2 = function _fireLogEvent2(level, message) {
  var context = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  if ((0, _classPrivateFieldGet2["default"])(this, _dispatcher)) {
    (0, _classPrivateFieldGet2["default"])(this, _dispatcher).dispatch(level, {
      level: level,
      message: message,
      context: context
    });
  }
};

var _resolve2 = function _resolve2(name) {
  if ((0, _classPrivateFieldGet2["default"])(this, _channels)[name] === undefined) {
    var _config$bubble;

    var config = (0, _classPrivateFieldGet2["default"])(this, _container).get('config').getThrough("logging.channels.".concat(name));

    if (config === null) {
      throw new _Exception["default"]("Log [".concat(name, "] is not defined."), 500, 'E_INVALID_ARGUMENT');
    }

    (0, _classPrivateFieldGet2["default"])(this, _channels)[name] = {
      bubble: (_config$bubble = config.bubble) !== null && _config$bubble !== void 0 ? _config$bubble : true,
      level: config.level,
      logger: new config.logger((0, _classPrivateFieldGet2["default"])(this, _container), _objectSpread(_objectSpread({}, config), {}, {
        logger: undefined
      }))
    };
  }

  return (0, _classPrivateFieldGet2["default"])(this, _channels)[name];
};

var _writeLog2 = function _writeLog2(level, message) {
  var _this = this;

  var context = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];

  _classPrivateMethodGet(this, _fireLogEvent, _fireLogEvent2).call(this, level, message, context);

  var continues = true;
  (0, _classPrivateFieldGet2["default"])(this, _activeChannels).forEach(function (name) {
    if (!continues) {
      return;
    }

    var channel = _classPrivateMethodGet(_this, _resolve, _resolve2).call(_this, name);

    if ((0, _classPrivateFieldGet2["default"])(_this, _levels)[channel.level] >= (0, _classPrivateFieldGet2["default"])(_this, _levels)[level]) {
      channel.logger[level](message, context);
      continues = channel.bubble;
    }
  }); // reset active channels

  (0, _classPrivateFieldSet2["default"])(this, _activeChannels, Object.keys((0, _classPrivateFieldGet2["default"])(this, _channels)));
};