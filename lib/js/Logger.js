"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _Exception = _interopRequireDefault(require("./Exception"));

var _container = new WeakMap();

var _options = new WeakMap();

var Logger =
/*#__PURE__*/
function () {
  (0, _createClass2["default"])(Logger, [{
    key: "alert",

    /**
     * The IoC container instance.
     * @type {Container|null}
     */

    /**
     * Logger options.
     * @type {object}
     */

    /**
     * Log an alert message to the logs.
     * @param message
     * @param context
     */
    value: function alert(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Create a new Logger instance.
     * @param {Application|Container} container
     * @param {object} options
     */

  }]);

  function Logger(container) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck2["default"])(this, Logger);

    _container.set(this, {
      writable: true,
      value: null
    });

    _options.set(this, {
      writable: true,
      value: {}
    });

    (0, _classPrivateFieldSet2["default"])(this, _container, container);
    (0, _classPrivateFieldSet2["default"])(this, _options, options);
  }
  /**
   * Log a critical message to the logs.
   * @param message
   * @param context
   */


  (0, _createClass2["default"])(Logger, [{
    key: "critical",
    value: function critical(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log a debug message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "debug",
    value: function debug(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log an emergency message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "emergency",
    value: function emergency(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log an error message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "error",
    value: function error(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log an informational message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "info",
    value: function info(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log a message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "log",
    value: function log(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log a notice to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "notice",
    value: function notice(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
    /**
     * Log a warning message to the logs.
     * @param message
     * @param context
     */

  }, {
    key: "warning",
    value: function warning(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }]);
  return Logger;
}();

exports["default"] = Logger;