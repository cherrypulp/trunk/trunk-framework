"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var LoggerInterface =
/*#__PURE__*/
function () {
  function LoggerInterface() {
    (0, _classCallCheck2["default"])(this, LoggerInterface);
  }

  (0, _createClass2["default"])(LoggerInterface, [{
    key: "formatMessage",
    value: function formatMessage(message) {
      return message;
    }
  }, {
    key: "emergency",
    value: function emergency(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "alert",
    value: function alert(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "critical",
    value: function critical(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "error",
    value: function error(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "warning",
    value: function warning(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "notice",
    value: function notice(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "info",
    value: function info(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "debug",
    value: function debug(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }, {
    key: "log",
    value: function log(message) {
      var context = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    }
  }]);
  return LoggerInterface;
}();

exports["default"] = LoggerInterface;