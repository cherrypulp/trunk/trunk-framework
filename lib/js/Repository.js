"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _Collection = _interopRequireDefault(require("./Collection"));

var Repository =
/*#__PURE__*/
function () {
  /**
   * All of the configuration items.
   * @type {Collection|{}}
   */

  /**
   * Create a new configuration repository.
   * @param {Object} items
   */
  function Repository() {
    var items = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    (0, _classCallCheck2["default"])(this, Repository);

    _items.set(this, {
      writable: true,
      value: {}
    });

    (0, _classPrivateFieldSet2["default"])(this, _items, new _Collection["default"](items));
  }
  /**
   * Get all of the configuration items for the application.
   * @return {Array|Object}
   */


  (0, _createClass2["default"])(Repository, [{
    key: "all",
    value: function all() {
      return (0, _classPrivateFieldGet2["default"])(this, _items).all();
    }
    /**
     * Get the specified configuration value.
     * @param {string} key
     * @param {*} defaultValue
     * @return {*}
     */

  }, {
    key: "get",
    value: function get(key) {
      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (Array.isArray(key)) {
        return this.getMany(key);
      }

      return (0, _classPrivateFieldGet2["default"])(this, _items).getThrough(key, defaultValue);
    }
    /**
     * Get many configuration values.
     * @param keys
     * @param {*} defaultValue
     * @return {*}
     */

  }, {
    key: "getMany",
    value: function getMany(keys) {
      var _this = this;

      var defaultValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return keys.map(function (key) {
        return _this.get(key, defaultValue);
      });
    }
    /**
     * Determine if the given configuration value exists.
     * @param {Array|String} keys
     * @return {boolean}
     */

  }, {
    key: "has",
    value: function has(keys) {
      return (0, _classPrivateFieldGet2["default"])(this, _items).hasThrough(keys);
    }
  }, {
    key: "merge",
    value: function merge(values) {
      (0, _classPrivateFieldGet2["default"])(this, _items).merge(values);
    }
  }, {
    key: "mergeRecursive",
    value: function mergeRecursive(values) {
      (0, _classPrivateFieldGet2["default"])(this, _items).mergeRecursive(values);
    }
    /**
     * Prepend a value onto an array configuration value.
     * @param {string} key
     * @param {*} value
     */

  }, {
    key: "prepend",
    value: function prepend(key, value) {
      var values = this.get(key);
      values = values.unshift(value);
      this.set(key, values);
    }
    /**
     * Push a value onto an array configuration value.
     * @param {string} key
     * @param {*} value
     */

  }, {
    key: "push",
    value: function push(key, value) {
      var values = this.get(key);
      values.push(value);
      this.set(key, values);
    }
    /**
     * Set a given configuration value.
     * @param {Array|String} keys
     * @param {*} value
     */

  }, {
    key: "set",
    value: function set(keys) {
      var _this2 = this;

      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (!Array.isArray(keys)) {
        keys = keys.split(' ');
      }

      keys.forEach(function (key) {
        (0, _classPrivateFieldGet2["default"])(_this2, _items).setThrough(key, value);
      });
    }
  }]);
  return Repository;
}();

exports["default"] = Repository;

var _items = new WeakMap();