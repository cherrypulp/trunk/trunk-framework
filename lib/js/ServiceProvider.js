"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var ServiceProvider =
/*#__PURE__*/
function () {
  /**
   * Application container.
   * @type {Application|null}
   */

  /**
   * Additional bindings.
   * @type {{}}
   */

  /**
   * Additional singletons.
   * @type {{}}
   */

  /**
   * Additional providers to load.
   * @type {Array}
   */

  /**
   * ServiceProvider constructor.
   * @param {Application|Container} container
   */
  function ServiceProvider(container) {
    (0, _classCallCheck2["default"])(this, ServiceProvider);
    (0, _defineProperty2["default"])(this, "app", null);
    (0, _defineProperty2["default"])(this, "bindings", {});
    (0, _defineProperty2["default"])(this, "singletons", {});
    (0, _defineProperty2["default"])(this, "providers", []);
    this.app = container;
  }
  /**
   * Boot.
   */


  (0, _createClass2["default"])(ServiceProvider, [{
    key: "boot",
    value: function boot() {}
    /**
     * Register.
     */

  }, {
    key: "register",
    value: function register() {}
    /**
     * Register provider and dependency providers.
     */

  }, {
    key: "registerProviders",
    value: function registerProviders() {
      var _this = this;

      this.providers.forEach(function (provider) {
        _this.app.register(provider);
      });
      this.register();
    }
  }]);
  return ServiceProvider;
}();

exports["default"] = ServiceProvider;