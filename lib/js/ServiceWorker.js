"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

/**
 * @TODO
 * - https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API
 * - https://developer.mozilla.org/fr/docs/Web/API/Service_Worker_API/Using_Service_Workers
 */
var ServiceWorker =
/*#__PURE__*/
function () {
  function ServiceWorker() {
    (0, _classCallCheck2["default"])(this, ServiceWorker);
  }

  (0, _createClass2["default"])(ServiceWorker, [{
    key: "on",
    value: function on(eventName, listener) {
      addEventListener(eventName, function (e) {
        return listener(e);
      });
    }
  }, {
    key: "activate",
    value: function activate() {}
  }, {
    key: "fetch",
    value: function fetch() {}
  }, {
    key: "install",
    value: function install() {}
  }, {
    key: "notify",
    value: function notify() {}
  }, {
    key: "message",
    value: function message() {}
  }]);
  return ServiceWorker;
}();

exports["default"] = ServiceWorker;