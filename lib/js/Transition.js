"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _waitUntilComplete = _interopRequireDefault(require("./helpers/waitUntilComplete"));

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

/**
 * Transition class.
 * @example
 *     class MyTransition extends Transition {
 *         name = 'my-transition';
 *
 *         in(done) {
 *             // do something...
 *             done();
 *         }
 *     }
 */
var Transition =
/*#__PURE__*/
function () {
  /**
   * Transition main DOM element.
   * @type {HTMLElement|String}
   */

  /**
   * Transition name.
   * @type {String}
   */

  /**
   * Transitions to run with the current one.
   * @type {*[]}
   */

  /**
   * @param element
   */
  function Transition() {
    var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.element;
    (0, _classCallCheck2["default"])(this, Transition);

    _doTransition.add(this);

    (0, _defineProperty2["default"])(this, "element", '.app');
    (0, _defineProperty2["default"])(this, "name", 'transition');
    (0, _defineProperty2["default"])(this, "transitions", []);
    this.element = element || this.element;

    if (typeof this.element === 'string') {
      this.element = document.querySelector(this.element);
    }
    /* eslint-disable-next-line */


    this.transitions.map(function (transition) {
      return new transition(element);
    });
  }
  /**
   * After enter hook.
   */


  (0, _createClass2["default"])(Transition, [{
    key: "afterEnter",
    value: function afterEnter() {}
    /**
     * After leave hook.
     */

  }, {
    key: "afterLeave",
    value: function afterLeave() {}
    /**
     * Before enter hook.
     */

  }, {
    key: "beforeEnter",
    value: function beforeEnter() {}
    /**
     * Before leave hook.
     */

  }, {
    key: "beforeLeave",
    value: function beforeLeave() {}
    /**
     * Do enter transitions.
     * @param {Function} done
     */

  }, {
    key: "doEnter",
    value: function doEnter() {
      var done = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      _classPrivateMethodGet(this, _doTransition, _doTransition2).call(this, 'in', done);
    }
    /**
     * Do leave transitions.
     * @param {Function} done
     */

  }, {
    key: "doLeave",
    value: function doLeave() {
      var done = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

      _classPrivateMethodGet(this, _doTransition, _doTransition2).call(this, 'out', done);
    }
    /**
     * Do transition with hooks.
     * @param {string} type
     * @param {Function} done
     */

  }, {
    key: "in",

    /**
     * Enter animation.
     * @param {Function} done
     */
    value: function _in(done) {
      done();
    }
    /**
     * Leave animation.
     * @param {Function} done
     */

  }, {
    key: "out",
    value: function out(done) {
      done();
    }
  }]);
  return Transition;
}();

exports["default"] = Transition;

var _doTransition = new WeakSet();

var _doTransition2 = function _doTransition2(type) {
  var _this = this;

  var done = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var className = "".concat(this.name, "-").concat(type);
  this.element.classList.add(className);
  this[type === 'in' ? 'beforeEnter' : 'beforeLeave']();
  this.transitions.forEach(function (transition) {
    (0, _waitUntilComplete["default"])(function (done) {
      return transition[type === 'in' ? 'doEnter' : 'doLeave'](done, transition);
    }, transition.name);
  });
  this[type](function () {
    return setTimeout(function () {
      if (typeof done === 'function') {
        done.call(context);
      }

      _this.element.classList.remove(className);
    }, 0);
  });
  this[type === 'in' ? 'afterEnter' : 'afterLeave']();
};