"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _ConsoleLogger = _interopRequireDefault(require("../loggers/ConsoleLogger"));

var _default = {
  channels: {
    // trigger logs via console
    console: {
      level: 'debug',
      // debug, info, notice, warning, error, critical, alert, emergency
      logger: _ConsoleLogger["default"]
    } // trigger logs via notification service
    // notification: {
    //     level: 'alert',
    // },
    //
    // // trigger logs via report service (AJAX)
    // report: {
    //     level: 'error',
    //     url: null,
    //     apikey: null,
    // },
    //
    // custom: {
    //     level: 'debug',
    //     // handler: 'CustomLogger',
    //     handler: {
    //         emergency(message, context = []) {},
    //         alert(message, context = []) {},
    //         critical(message, context = []) {},
    //         error(message, context = []) {},
    //         warning(message, context = []) {},
    //         notice(message, context = []) {},
    //         info(message, context = []) {},
    //         debug(message, context = []) {},
    //         log(message, context = []) {},
    //     },
    //     formatter: 'CustomFormatter',
    // },

  }
};
exports["default"] = _default;