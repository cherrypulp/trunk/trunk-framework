"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = decorate;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _toArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toArray"));

var _decorate2 = require("../helpers/decorate");

var defineProperty = Object.defineProperty;

function handleDescriptor(target, key, descriptor, _ref) {
  var _ref2 = (0, _toArray2["default"])(_ref),
      decorator = _ref2[0],
      args = _ref2.slice(1);

  var configurable = descriptor.configurable,
      enumerable = descriptor.enumerable,
      writable = descriptor.writable;
  var originalGet = descriptor.get;
  var originalSet = descriptor.set;
  var originalValue = descriptor.value;
  var isGetter = !!originalGet;
  return {
    configurable: configurable,
    enumerable: enumerable,
    get: function get() {
      var fn = isGetter ? originalGet.call(this) : originalValue;
      var value = decorator.call.apply(decorator, [this, fn].concat((0, _toConsumableArray2["default"])(args)));

      if (isGetter) {
        return value;
      }

      var desc = {
        configurable: configurable,
        enumerable: enumerable
      };
      desc.value = value;
      desc.writable = writable;
      defineProperty(this, key, desc);
      return value;
    },
    set: isGetter ? originalSet : (0, _decorate2.createDefaultSetter)()
  };
}
/**
 * Immediately applies the provided function and arguments to the method.
 * The first argument is the function to apply, all further arguments will be passed to that decorating function.
 * @example
 * class Foo {
 *     \@decorate(memoize)
 *     foo() {}
 * }
 * @param args
 * @return {function(): *}
 */


function decorate() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return (0, _decorate2.decorate)(handleDescriptor, args);
}