"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = deprecated;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _decorate = require("../helpers/decorate");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var DEFAULT_MSG = 'This function will be removed in future versions.';

function handleDescriptor(target, key, descriptor, _ref) {
  var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
      _ref2$ = _ref2[0],
      msg = _ref2$ === void 0 ? DEFAULT_MSG : _ref2$,
      _ref2$2 = _ref2[1],
      options = _ref2$2 === void 0 ? {} : _ref2$2;

  if (typeof descriptor.value !== 'function') {
    throw new SyntaxError('Only functions can be marked as deprecated');
  }

  var methodSignature = "".concat(target.constructor.name, "#").concat(key);

  if (options.url) {
    msg += "\n\n    See ".concat(options.url, " for more details.\n\n");
  }

  return _objectSpread(_objectSpread({}, descriptor), {}, {
    value: function deprecationWrapper() {
      console.warn('%cDEPRECATION%c %s', 'background-color: #e88388; color: #fff;', '', "".concat(methodSignature, ": ").concat(msg));
      return descriptor.value.apply(this, arguments);
    }
  });
}
/**
 * Calls console.warn() with a deprecation message. Provide a custom message to override the default one.
 * You can also provide an options hash with a url, for further reading.
 * @param args
 * @return {function(): *}
 */


function deprecated() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return (0, _decorate.decorate)(handleDescriptor, args);
}