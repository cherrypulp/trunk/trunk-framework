"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.inherit = inherit;

/**
 * Inherit from multiple classes.
 * @param args
 * @return {function(*=): *}
 */
function inherit() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  var classes = args.reverse();
  return function (target) {
    classes.forEach(function (klass) {
      var keys = Object.getOwnPropertyNames(klass.prototype);
      keys.forEach(function (key) {
        if (!target.prototype[key] && typeof klass.prototype[key] === 'function') {
          target.prototype[key] = klass.prototype[key];
        }
      });
    });
    return target;
  };
}