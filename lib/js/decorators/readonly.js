"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = readonly;

var _decorate = require("../helpers/decorate");

function handleDescriptor(target, key, descriptor) {
  descriptor.writable = false;
  return descriptor;
}
/**
 * Marks a property or method as not being writable.
 * @param args
 * @return {function(): *}
 */


function readonly() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return (0, _decorate.decorate)(handleDescriptor, args);
}