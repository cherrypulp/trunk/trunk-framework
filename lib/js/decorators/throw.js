"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.exception = exception;

/**
 * Catch an error in the given function and throw a given Exception.
 * @param except
 * @TODO
 * @see https://github.com/AvraamMavridis/javascript-decorators/blob/master/src/%40trycatch.js
 */
function exception(except) {
  if (except.constructor.name) {}
}