"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = transition;

/**
 * @example
 *     \@transition(DefaultTransition)
 *     class FooController {
 *         // ...
 *     }
 * @param {Transition} transition
 * @return {function(*): *}
 */
function transition() {
  var transition = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  return function (target) {
    target.prototype.transition = transition;
    return target;
  };
}