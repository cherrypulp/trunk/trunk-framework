"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _Logger2 = _interopRequireDefault(require("../Logger"));

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

function _classStaticPrivateMethodGet(receiver, classConstructor, method) { if (receiver !== classConstructor) { throw new TypeError("Private static access of wrong provenance"); } return method; }

var colors = {
  black: '#000',
  blue: '#71bef2',
  cyan: '#66c2cd',
  green: '#a9cc8d',
  pink: '#d291e4',
  red: '#e88388',
  white: '#fff',
  yellow: '#dbab79'
};
var styles = {
  emergency: "background-color: ".concat(colors.ping, "; color: ").concat(colors.black),
  alert: "color: ".concat(colors.ping),
  critical: "background-color: ".concat(colors.red, "; color: ").concat(colors.black),
  error: "color: ".concat(colors.red),
  warning: "color: ".concat(colors.yellow),
  notice: "background-color: ".concat(colors.cyan, "; color: ").concat(colors.black),
  info: "color: ".concat(colors.cyan),
  debug: "color: ".concat(colors.blue),
  log: ''
};

var ConsoleLogger =
/*#__PURE__*/
function (_Logger) {
  (0, _inherits2["default"])(ConsoleLogger, _Logger);

  var _super = _createSuper(ConsoleLogger);

  function ConsoleLogger() {
    (0, _classCallCheck2["default"])(this, ConsoleLogger);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(ConsoleLogger, [{
    key: "emergency",

    /**
     * Format message to display in console.
     * @param level
     * @param message
     * @param context
     */

    /**
     * Emergency.
     * @param message
     * @param context
     */
    value: function emergency(message) {
      for (var _len = arguments.length, context = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        context[_key - 1] = arguments[_key];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'emergency', message, context);
    }
    /**
     * Alert.
     * @param message
     * @param context
     */

  }, {
    key: "alert",
    value: function alert(message) {
      for (var _len2 = arguments.length, context = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
        context[_key2 - 1] = arguments[_key2];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'alert', message, context);
    }
    /**
     * Critical.
     * @param message
     * @param context
     */

  }, {
    key: "critical",
    value: function critical(message) {
      for (var _len3 = arguments.length, context = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
        context[_key3 - 1] = arguments[_key3];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'critical', message, context);
    }
    /**
     * Error.
     * @param message
     * @param context
     */

  }, {
    key: "error",
    value: function error(message) {
      for (var _len4 = arguments.length, context = new Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
        context[_key4 - 1] = arguments[_key4];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'error', message, context);
    }
    /**
     * Warning.
     * @param message
     * @param context
     */

  }, {
    key: "warning",
    value: function warning(message) {
      for (var _len5 = arguments.length, context = new Array(_len5 > 1 ? _len5 - 1 : 0), _key5 = 1; _key5 < _len5; _key5++) {
        context[_key5 - 1] = arguments[_key5];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'warning', message, context);
    }
    /**
     * Notice.
     * @param message
     * @param context
     */

  }, {
    key: "notice",
    value: function notice(message) {
      for (var _len6 = arguments.length, context = new Array(_len6 > 1 ? _len6 - 1 : 0), _key6 = 1; _key6 < _len6; _key6++) {
        context[_key6 - 1] = arguments[_key6];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'notice', message, context);
    }
    /**
     * Info.
     * @param message
     * @param context
     */

  }, {
    key: "info",
    value: function info(message) {
      for (var _len7 = arguments.length, context = new Array(_len7 > 1 ? _len7 - 1 : 0), _key7 = 1; _key7 < _len7; _key7++) {
        context[_key7 - 1] = arguments[_key7];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'info', message, context);
    }
    /**
     * Debug.
     * @param message
     * @param context
     */

  }, {
    key: "debug",
    value: function debug(message) {
      for (var _len8 = arguments.length, context = new Array(_len8 > 1 ? _len8 - 1 : 0), _key8 = 1; _key8 < _len8; _key8++) {
        context[_key8 - 1] = arguments[_key8];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'debug', message, context);
    }
    /**
     * Log.
     * @param message
     * @param context
     */

  }, {
    key: "log",
    value: function log(message) {
      for (var _len9 = arguments.length, context = new Array(_len9 > 1 ? _len9 - 1 : 0), _key9 = 1; _key9 < _len9; _key9++) {
        context[_key9 - 1] = arguments[_key9];
      }

      _classStaticPrivateMethodGet(ConsoleLogger, ConsoleLogger, _writeLog).call(ConsoleLogger, 'log', message, context);
    }
  }]);
  return ConsoleLogger;
}(_Logger2["default"]);

exports["default"] = ConsoleLogger;

var _writeLog = function _writeLog(level, message, context) {
  var _console;

  var date = new Date();
  var format = "%c[".concat("0".concat(date.getHours()).slice(-2), ":").concat(date.getMinutes(), ":").concat(date.getSeconds(), ".").concat(date.getMilliseconds(), "] %c[").concat(level.toUpperCase(), "] ");

  if (typeof message === 'string') {
    format += '%s';
  } else {
    format += '%o';
  }

  (_console = console).log.apply(_console, [format, 'font-size: .8em', styles[level], message].concat((0, _toConsumableArray2["default"])(context)));
};