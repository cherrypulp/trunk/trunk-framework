"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

var _Collection = _interopRequireDefault(require("../Collection"));

var _vue = _interopRequireDefault(require("./../config/vue.js"));

var _logging = _interopRequireDefault(require("./../config/logging.js"));

var _app = _interopRequireDefault(require("./../config/app.js"));

var _require = require("../helpers/require");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

var baseConfigs = {};
baseConfigs["Vue"] = _vue["default"];
baseConfigs["Logging"] = _logging["default"];
baseConfigs["App"] = _app["default"];

var ConfigServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(ConfigServiceProvider, _ServiceProvider);

  var _super = _createSuper(ConfigServiceProvider);

  function ConfigServiceProvider() {
    (0, _classCallCheck2["default"])(this, ConfigServiceProvider);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(ConfigServiceProvider, [{
    key: "register",

    /**
     * Register.
     */
    value: function register() {
      // add dependency to the container
      this.app.singleton('config', function () {
        return new _Collection["default"]({});
      });

      try {
        var config = this.app.get('config'); // load default configurations

        Object.entries(baseConfigs).forEach(function (_ref) {
          var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
              key = _ref2[0],
              value = _ref2[1];

          config.put(key.toLowerCase(), value);
        }); // load application configurations
        // @note - webpack need require here to link with folder name

        var appConfigs = (0, _require.mapRequiredFiles)(require.context('@/js', true, /config\/.*\.js$/));
        Object.entries(appConfigs).forEach(function (_ref3) {
          var _ref4 = (0, _slicedToArray2["default"])(_ref3, 2),
              key = _ref4[0],
              value = _ref4[1];

          key = key.toLowerCase(); // @TODO - merge recursively

          config.put(key, _objectSpread(_objectSpread({}, config.get(key)), value));
        });
      } catch (e) {
        // console.error(e);
        throw e;
      }
    }
  }]);
  return ConfigServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = ConfigServiceProvider;