"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _classPrivateFieldSet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldSet"));

var _classPrivateFieldGet2 = _interopRequireDefault(require("@babel/runtime/helpers/classPrivateFieldGet"));

var _Dispatcher = _interopRequireDefault(require("../Dispatcher"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

var _require = require("../helpers/require");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var _controllers = new WeakMap();

var _fireController = new WeakSet();

var _getControllers = new WeakSet();

var ControllerServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(ControllerServiceProvider, _ServiceProvider);

  var _super = _createSuper(ControllerServiceProvider);

  function ControllerServiceProvider() {
    var _this;

    (0, _classCallCheck2["default"])(this, ControllerServiceProvider);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));

    _getControllers.add((0, _assertThisInitialized2["default"])(_this));

    _fireController.add((0, _assertThisInitialized2["default"])(_this));

    _controllers.set((0, _assertThisInitialized2["default"])(_this), {
      writable: true,
      value: {
        // @note - ensure DefaultController is the first one booted
        DefaultController: null
      }
    });

    return _this;
  }

  (0, _createClass2["default"])(ControllerServiceProvider, [{
    key: "register",

    /**
     * Register.
     */
    value: function register() {
      var _this2 = this;

      // load application controllers
      (0, _classPrivateFieldSet2["default"])(this, _controllers, _classPrivateMethodGet(this, _getControllers, _getControllers2).call(this)); // Register controllers then create if needed.

      Object.entries((0, _classPrivateFieldGet2["default"])(this, _controllers)).forEach(function (_ref) {
        var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
            name = _ref2[0],
            controller = _ref2[1];

        _this2.app.singleton(name, function () {
          /* eslint-disable-next-line */
          return new controller(_this2.app);
        });

        _this2.app["with"](name, function (instance) {
          return _classPrivateMethodGet(_this2, _fireController, _fireController2).call(_this2, instance, 'doCreated');
        });
      });
    }
    /**
     * Boot.
     */

  }, {
    key: "boot",
    value: function boot() {
      var _this3 = this;

      Object.keys((0, _classPrivateFieldGet2["default"])(this, _controllers)).forEach(function (name) {
        _this3.app["with"](name, function (instance) {
          return _classPrivateMethodGet(_this3, _fireController, _fireController2).call(_this3, instance, 'doMounted');
        });
      });
    }
  }]);
  return ControllerServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = ControllerServiceProvider;

var _fireController2 = function _fireController2(instance) {
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'doCreated';

  if (instance.isActive()) {
    instance[action]();
  }
};

var _getControllers2 = function _getControllers2() {
  // @note - webpack need require here to link with folder name
  var controllers = (0, _require.mapRequiredFiles)(require.context('@/js', true, /controllers\/.*\.js$/));
  return _objectSpread(_objectSpread({}, (0, _classPrivateFieldGet2["default"])(this, _controllers)), controllers);
};