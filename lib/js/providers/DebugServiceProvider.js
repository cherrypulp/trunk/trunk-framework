"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

var DebugServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(DebugServiceProvider, _ServiceProvider);

  function DebugServiceProvider() {
    (0, _classCallCheck2["default"])(this, DebugServiceProvider);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(DebugServiceProvider).apply(this, arguments));
  }

  (0, _createClass2["default"])(DebugServiceProvider, [{
    key: "register",
    value: function register() {// ...
    }
  }]);
  return DebugServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = DebugServiceProvider;