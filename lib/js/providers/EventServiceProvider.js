"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

var _Dispatcher = _interopRequireDefault(require("../Dispatcher"));

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

var EventServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(EventServiceProvider, _ServiceProvider);

  var _super = _createSuper(EventServiceProvider);

  function EventServiceProvider() {
    (0, _classCallCheck2["default"])(this, EventServiceProvider);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(EventServiceProvider, [{
    key: "register",

    /**
     * Register events.
     */
    value: function register() {
      var _this = this;

      this.app.singleton('events', function () {
        return new _Dispatcher["default"](_this.app);
      });
    }
  }]);
  return EventServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = EventServiceProvider;