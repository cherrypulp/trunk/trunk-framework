"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

var _LogManager = _interopRequireDefault(require("../LogManager"));

var _EventServiceProvider = _interopRequireDefault(require("./EventServiceProvider"));

var _Exception = _interopRequireDefault(require("../Exception"));

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

var LogServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(LogServiceProvider, _ServiceProvider);

  var _super = _createSuper(LogServiceProvider);

  function LogServiceProvider() {
    var _this;

    (0, _classCallCheck2["default"])(this, LogServiceProvider);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "providers", [new _EventServiceProvider["default"](_this.app)]);
    return _this;
  }

  (0, _createClass2["default"])(LogServiceProvider, [{
    key: "register",

    /**
     * Register Logger.
     */
    value: function register() {
      if (window !== undefined) {
        // catch uncaught errors
        window.onerror = function (errorMsg, url, lineNumber, colNumber, error) {
          if (error instanceof _Exception["default"] || error instanceof Error) {
            this.app.get('log').critical("[".concat(error.name, "] ").concat(error.message, " with code ").concat(error.code, " at line ").concat(lineNumber, ":").concat(colNumber, " in ").concat(url, "\n"), error.stack);
          } else {
            this.app.get('log').critical("".concat(errorMsg, " at line ").concat(lineNumber, ":").concat(colNumber, " in ").concat(url, " (").concat(error.stack, ")"));
          }

          return true;
        };
      }

      this.app.singleton('log', function (app) {
        return new _LogManager["default"](app);
      });
    }
  }]);
  return LogServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = LogServiceProvider;