"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

// import barba from '@barba/core';
var TransitionAjaxProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(TransitionAjaxProvider, _ServiceProvider);

  function TransitionAjaxProvider() {
    (0, _classCallCheck2["default"])(this, TransitionAjaxProvider);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(TransitionAjaxProvider).apply(this, arguments));
  }

  (0, _createClass2["default"])(TransitionAjaxProvider, [{
    key: "register",
    value: function register() {
      console.log('TransitionAjaxProvider.register ->', this.app); // try {
      //     barba.hooks.before(() => {
      //         barba.wrapper.classList.add('is-animating');
      //     });
      //
      //     barba.hooks.after(() => {
      //         barba.wrapper.classList.remove('is-animating');
      //     });
      //
      //     // barba.init({
      //     //     // transitions,
      //     // });
      // } catch (e) {
      //     console.error(e);
      // }
    }
  }, {
    key: "boot",
    value: function boot() {
      console.log('TransitionAjaxProvider.boot ->', this.app);
    }
  }]);
  return TransitionAjaxProvider;
}(_ServiceProvider2["default"]); // toBarbaJSTransition() {
//     return {
//         name: this.name,
//
//         // apply only when leaving `[data-barba-namespace="home"]`
//         from: 'home',
//
//         // apply only when transitioning to `[data-barba-namespace="products | contact"]`
//         to: {
//             namespace: ['products', 'contact'],
//         },
//
//         // apply only if clicked link contains `.cta`
//         custom({ current, next, trigger }) {
//             return (
//                 trigger.classList &&
//                 trigger.classList.contains(this.element)
//             );
//         },
//
//         // do leave and enter concurrently
//         sync: true,
//
//         // available hooks…
//         beforeOnce() {},
//         once() {},
//         afterOnce() {},
//
//         beforeLeave() {},
//         leave() {},
//         afterLeave() {},
//
//         beforeEnter() {},
//         enter() {},
//         afterEnter() {},
//     };
// }


exports["default"] = TransitionAjaxProvider;