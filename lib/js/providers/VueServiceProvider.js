"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _vue = _interopRequireDefault(require("vue"));

var _ServiceProvider2 = _interopRequireDefault(require("../ServiceProvider"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) {
  function isNativeReflectConstruct() {
    if (typeof Reflect === "undefined" || !Reflect.construct) return false;
    if (Reflect.construct.sham) return false;
    if (typeof Proxy === "function") return true;

    try {
      Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
      return true;
    } catch (e) {
      return false;
    }
  }

  return function () {
    var Super = (0, _getPrototypeOf2["default"])(Derived),
        result;

    if (isNativeReflectConstruct()) {
      var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor;
      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return (0, _possibleConstructorReturn2["default"])(this, result);
  };
}

var VueServiceProvider =
/*#__PURE__*/
function (_ServiceProvider) {
  (0, _inherits2["default"])(VueServiceProvider, _ServiceProvider);

  var _super = _createSuper(VueServiceProvider);

  function VueServiceProvider() {
    (0, _classCallCheck2["default"])(this, VueServiceProvider);
    return _super.apply(this, arguments);
  }

  (0, _createClass2["default"])(VueServiceProvider, [{
    key: "register",
    value: function register() {
      /**
       * @example
       * this.app.make('Vue', {
       *     el: '.app',
       * });
       */
      this.app.bind('Vue', function (app) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

        if (options) {
          return new _vue["default"](options);
        }

        return _vue["default"];
      });
    }
  }, {
    key: "boot",
    value: function boot() {
      var _this = this;

      var config = _objectSpread({
        el: null,
        components: {},
        directives: {},
        mixins: [],
        options: {},
        plugins: []
      }, this.app.get('config').get('vue'));

      this.app.get('Vue').config.errorHandler = function (error, vue, info) {
        if (error instanceof Error) {
          _this.app.get('log').critical("".concat(error.name, " thrown with \"").concat(error.message, "\" at ").concat(info, "\n"), error.stack, '\n', vue);
        } else {
          _this.app.get('log').critical("\"".concat(error, "\" at ").concat(info, "\n"), '\n', vue);
        }
      };

      config.plugins.forEach(function (value) {
        if (Array.isArray(value)) {
          var _value$;

          _this.app.get('Vue').use(value[0], (_value$ = value[1]) !== null && _value$ !== void 0 ? _value$ : null);
        } else {
          _this.app.get('Vue').use(value);
        }
      });
      config.mixins.forEach(function (value) {
        _this.app.get('Vue').mixin(value);
      });
      Object.entries(config.components).forEach(function (_ref) {
        var _ref2 = (0, _slicedToArray2["default"])(_ref, 2),
            key = _ref2[0],
            value = _ref2[1];

        _this.app.get('Vue').component(key, value);
      });
      Object.entries(config.directives).forEach(function (_ref3) {
        var _ref4 = (0, _slicedToArray2["default"])(_ref3, 2),
            key = _ref4[0],
            value = _ref4[1];

        _this.app.get('Vue').directive(key, value);
      });

      if (config.el) {
        this.app.make('Vue', _objectSpread({
          el: config.el
        }, config.options));
      }
    }
  }]);
  return VueServiceProvider;
}(_ServiceProvider2["default"]);

exports["default"] = VueServiceProvider;