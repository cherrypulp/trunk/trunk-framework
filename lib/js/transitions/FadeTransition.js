"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _Transition2 = _interopRequireDefault(require("../Transition"));

function _classPrivateMethodGet(receiver, privateSet, fn) { if (!privateSet.has(receiver)) { throw new TypeError("attempted to get private field on non-instance"); } return fn; }

var FadeTransition =
/*#__PURE__*/
function (_Transition) {
  (0, _inherits2["default"])(FadeTransition, _Transition);

  function FadeTransition() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2["default"])(this, FadeTransition);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2["default"])(this, (_getPrototypeOf2 = (0, _getPrototypeOf3["default"])(FadeTransition)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _getOrCreateOverlay.add((0, _assertThisInitialized2["default"])(_this));

    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "name", 'fade');
    return _this;
  }

  (0, _createClass2["default"])(FadeTransition, [{
    key: "in",

    /**
     * Enter animation.
     * @param done
     */
    value: function _in(done) {
      _classPrivateMethodGet(this, _getOrCreateOverlay, _getOrCreateOverlay2).call(this);

      done();
    }
  }, {
    key: "out",
    value: function out(done) {
      done();
    }
  }]);
  return FadeTransition;
}(_Transition2["default"]);

exports["default"] = FadeTransition;

var _getOrCreateOverlay = new WeakSet();

var _getOrCreateOverlay2 = function _getOrCreateOverlay2() {
  var className = "".concat(this.name, "-overlay");
  var overlay = document.querySelector(".".concat(className));

  if (!overlay) {
    overlay = document.createElement('div');
    overlay.className = className;
    this.element.appendChild(overlay);
  }

  return overlay;
};