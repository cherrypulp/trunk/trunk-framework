import Container from './Container';
import AppServiceProvider from './providers/AppServiceProvider';

export default class Application extends Container {
    /**
     * The current globally available container (if any).
     * @type {Container|null}
     */
    static instance = null;

    /**
     * Indicates if the application has booted.
     * @type {boolean}
     */
    #booted = false;

    /**
     * The names of the loaded service providers.
     * @type {Map<string, boolean>}
     */
    #loadedProviders = new Map();

    /**
     * All of the registered service providers.
     * @type {Map<string, ServiceProvider>}
     */
    #serviceProviders = new Map();

    /**
     * Set the globally available instance of the container.
     * @param {Application, Container, string} className
     * @return {Container}
     */
    static getInstance(className = Application) {
        return Container.getInstance(className);
    }

    /**
     * Set the shared instance of the container.
     * @param {Container} container
     * @param {Application, Container, string} className
     * @return {Container}
     */
    static setInstance(container, className = Application) {
        return Container.setInstance(container, className);
    }

    /**
     * Application constructor.
     */
    constructor() {
        super();

        this.#registerBaseBindings();
        this.#registerBaseServiceProviders();
    }

    /**
     * Boot the application's service providers.
     */
    boot() {
        if (this.#booted) {
            return;
        }

        this.#serviceProviders.forEach((provider) => {
            this.#bootProvider(provider);
        });

        this.#booted = true;
    }

    /**
     * Boot the given service provider.
     * @param {ServiceProvider} provider
     * @return {*}
     */
    #bootProvider(provider) {
        if (typeof provider.boot === 'function') {
            return provider.boot();
        }
    }

    /**
     * Get the registered service provider instance if it exists.
     * @param {string} provider
     * @return {ServiceProvider|null}
     */
    getProvider(provider) {
        if (this.#serviceProviders.has(provider)) {
            return this.#serviceProviders.get(provider);
        }

        return null;
    }

    /**
     * Mark the given provider as registered.
     * @param {ServiceProvider} provider
     */
    #markAsRegistered(provider) {
        this.#serviceProviders.set(provider.constructor.name, provider);
        this.#loadedProviders.set(provider.constructor.name, true);
    }

    /**
     * Register a service provider with the application.
     * @param {ServiceProvider|String} provider
     * @param {array} options
     * @param {boolean} force
     */
    register(provider, options = [], force = false) {
        const registered = this.getProvider(provider);

        if (registered && !force) {
            return registered;
        }

        if (typeof provider === 'string') {
            provider = this.resolveProvider(provider);
        }

        // Set app instance if not already set
        if (!provider.app) {
            provider.app = this;
        }

        provider.registerProviders();

        if (typeof provider.bindings !== 'undefined') {
            Object.entries(provider.bindings).forEach(([key, value]) => {
                this.bind(key, value);
            });
        }

        if (typeof provider.singletons !== 'undefined') {
            Object.entries(provider.singletons).forEach(([key, value]) => {
                this.singleton(key, value);
            });
        }

        this.#markAsRegistered(provider);

        if (this.#booted) {
            this.#bootProvider(provider);
        }

        return provider;
    }

    /**
     * Register base bindings.
     */
    #registerBaseBindings() {
        Application.setInstance(this);
        this.instance('app', this);
        this.instance('Container', this);
    }

    /**
     * Register base service providers.
     */
    #registerBaseServiceProviders() {
        this.register(new AppServiceProvider(this));
    }

    /**
     * Resolve a service provider instance from the classs.
     * @param {string} provider
     * @return {ServiceProvider|null}
     */
    resolveProvider(provider) {
        if (this.#serviceProviders.has(provider)) {
            /* eslint-disable-next-line */
            provider = this.#serviceProviders.get(provider);
            return provider;
        }

        return null;
    }

    /**
     * Add a shortcut to the app instance.
     * @param dependencyName
     * @param customName
     * @param descriptor
     */
    shortcut(dependencyName, customName = null, descriptor = {}) {
        if (!customName) {
            customName = dependencyName;
        }

        Object.defineProperty(this, customName, {
            configurable: true,
            enumerable: true,
            writable: false,
            value: this.use(dependencyName),
            ...descriptor,
        });
    }
}
