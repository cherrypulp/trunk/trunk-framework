import collect from 'collect.js';

export default class Collection extends collect {
    /**
     * @param items
     */
    constructor(items) {
        super(items);

        /**
         * Get value through dot notation path.
         * @param key
         * @param defaultValue
         * @return {*}
         */
        this.getThrough = (key, defaultValue = null) => {
            try {
                return (
                    key
                        .split('.')
                        .reduce((acc, prop) => acc[prop], this.items) ||
                    defaultValue
                );
            } catch (err) {
                return defaultValue;
            }
        };

        /**
         * Check if a key exists through path.
         * @param keys
         * @return {boolean}
         */
        this.hasThrough = (keys) => {
            if (!Array.isArray(keys)) {
                keys = keys.split(' ');
            }

            return (
                keys.filter((key) => this.getThrough(key)).length ===
                keys.length
            );
        };

        /**
         * Set given value through dot notation path.
         * @param key
         * @param value
         * @return {*}
         */
        this.setThrough = (key, value) => {
            const keys = key.split('.');
            let source = this.items;

            for (let i = 0, len = keys.length; i < len; i++) {
                const k = keys[i];

                if (i === keys.length - 1) {
                    source[k] = value;
                }

                if (source[k] === undefined) {
                    source[k] = {};
                }

                source = source[k];
            }

            return this;
        };
    }
}
