import Exception from './Exception';

export default class Container {
    /**
     * The current globally available container (if any).
     * @type {Container|null}
     */
    static instance = null;

    /**
     * The registered type aliases.
     * @type {Map<String, String>}
     */
    #aliases = new Map();

    /**
     * The container's bindings.
     * @type {Map<String, Object>}
     */
    #bindings = new Map();

    /**
     * An array of the types that have been resolved.
     * @type {Map<String, Boolean>}
     */
    #resolved = new Map();

    /**
     * Set the globally available instance of the container.
     * @param {Container} className
     * @return {Container}
     */
    static getInstance(className = Container) {
        if (className.instance === null) {
            /* eslint-disable-next-line */
            className.instance = new className();
        }

        return className.instance;
    }

    /**
     * Set the shared instance of the container.
     * @param {Container} container
     * @param className
     * @return {Container}
     */
    static setInstance(container, className = Container) {
        return (className.instance = container);
    }

    /**
     * Alias a type to a different name.
     * Keeping the alias unique is the responsibility of the user.
     * @example
     * ```js
     * ioc.alias('App/User', 'user');
     * ```
     * @param {string} namespace
     * @param {string} alias
     */
    alias(namespace, alias) {
        this.#aliases.set(alias, namespace);
    }

    /**
     * Register a binding with the container.
     * Keeping the namespace unique is the responsibility of the user.
     * @example
     * ```js
     * ioc.bind('App/User', () => new User());
     * ```
     * @param {string} namespace
     * @param {Function|String|null} callback
     */
    bind(namespace, callback = null) {
        if (typeof callback !== 'function') {
            const _callback = callback;
            callback = () => _callback;
        }

        this.#bindings.set(namespace, {
            namespace,
            callback,
            singleton: false,
        });
    }

    /**
     * @param {string} namespace
     * @return {*}
     */
    get(namespace) {
        return this.resolve(namespace);
    }

    /**
     * Get the alis for an abstract if available.
     * @param {string} namespace
     * @return {String}
     */
    getAlias(namespace) {
        if (this.#aliases.has(namespace)) {
            return this.#aliases.get(namespace);
        }

        return namespace;
    }

    /**
     * @example
     * ```js
     * ioc.has('App/User');     // namespace
     * ioc.has('user', true);   // alias
     * ```
     * @param {string} namespace
     * @param {boolean} checkAliases
     * @return {boolean}
     */
    has(namespace, checkAliases = false) {
        if (checkAliases) {
            return this.#bindings.has(this.getAlias(namespace));
        }

        return this.#bindings.has(namespace);
    }

    /**
     * Register an existing instance as shared in the container.
     * @example
     * ```js
     * ioc.instance('app', this);
     * ```
     * @param namespace
     * @param instance
     */
    instance(namespace, instance) {
        namespace = this.getAlias(namespace);

        let binding = {
            cachedValue: instance,
            callback: null,
            namespace,
            singleton: true,
        };

        if (this.#bindings.has(namespace)) {
            binding = {
                ...this.#bindings.get(namespace),
                cachedValue: instance,
            };
        }

        this.#bindings.set(namespace, binding);
    }

    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ioc.make('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.make('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.make('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */
    make(namespace, parameters = []) {
        return this.resolve(namespace, parameters);
    }

    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ioc.resolve('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.resolve('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.resolve('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */
    resolve(namespace, parameters = []) {
        namespace = this.getAlias(namespace);

        if (!this.has(namespace)) {
            throw new Exception(
                `Cannot resolve ${namespace} binding from the IoC Container`,
                500,
                'E_IOC_BINDING_NOT_FOUND',
            );
        }

        const binding = this.#bindings.get(namespace);

        if (binding.singleton) {
            if (binding.cachedValue === undefined) {
                binding.cachedValue = parameters.length
                    ? binding.callback(this, ...parameters)
                    : binding.callback(this);
            }

            this.#resolved.set(namespace, true);
            return binding.cachedValue;
        }

        this.#resolved.set(namespace, true);
        return parameters.length
            ? binding.callback(this, ...parameters)
            : binding.callback(this);
    }

    /**
     * Register a shared binding in the container.
     * The callback will be invoked only once.
     * @example
     * ```js
     * ioc.singleton('App/User', () => new User());
     * ```
     * @param {string} namespace
     * @param {*} callback
     */
    singleton(namespace, callback = null) {
        if (typeof callback !== 'function') {
            const _callback = callback;
            callback = () => _callback;
        }

        this.#bindings.set(namespace, {
            namespace,
            callback,
            singleton: true,
        });
    }

    /**
     * Resolve the given type from the container.
     * @example
     * ```js
     * ios.use('user');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (app) => {
     *     const activeUser = app.get('Acitve/User');
     *     return new User(activeUser);
     * });
     * ioc.use('App/User');
     * ```
     * @example
     * ```js
     * ioc.bind('App/User', (firstname, lastname) => {
     *     return new User({
     *         firstname,
     *         lastname,
     *     });
     * });
     * ioc.use('App/User', 'John', 'Doe');
     * ```
     * @param {string} namespace
     * @param {array} parameters
     * @return {*}
     */
    use(namespace, parameters = []) {
        return this.resolve(namespace, parameters);
    }

    /**
     * Execute a callback by resolving bindings from the container.
     * Only executed when all bindings exists in the container.
     * @example
     * ```js
     * ioc.with(['App/User'], (User) => {
     *     // do something with User...
     * });
     * ```
     * @param {array} namespaces
     * @param {Function|null} callback
     */
    with(namespaces, callback = null) {
        if (typeof namespaces === 'string') {
            namespaces = [namespaces];
        }

        if (namespaces.every((namespace) => this.has(namespace, true))) {
            callback(...namespaces.map((namespace) => this.use(namespace)));
        }
    }
}
