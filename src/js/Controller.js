/**
 * Controller class to use instead of @dogstudio/highway Rendered class.
 * @example
 * class MyController extends Controller {
 *     // do something...
 * }
 */
export default class Controller {
    /**
     * Application container.
     * @type {Application|Container|null}
     */
    app = null;

    /**
     * Controller name.
     * @type {null}
     */
    name = null;

    /**
     * Controller constructor.
     * @param {Application|Container} container
     */
    constructor(container) {
        this.app = container;
        this.doBeforeCreate();
    }

    /**
     * @return {*}
     */
    isActive() {
        return document.body.className
            .toLowerCase()
            .split(/\s+/)
            .includes(this.#getName());
    }

    /**
     * Executed when ControllerProvider instantiate controllers.
     */
    beforeCreate() {}

    /**
     * Executed on ServiceProvider register.
     */
    created() {}

    /**
     * Do internal helper.
     * @param {string} type
     */
    #do(type) {
        this[type]();
        this.#trigger(type);
    }

    /**
     * Do beforeCreate callback and trigger an event.
     */
    doBeforeCreate() {
        this.#do('beforeCreate');
    }

    /**
     * Do created callback and trigger an event.
     */
    doCreated() {
        this.#do('created');
    }

    /**
     * Do mounted callback and trigger an event.
     */
    doMounted() {
        this.#do('mounted');
    }

    /**
     * Get name value.
     * @return {*}
     */
    #getName() {
        return this.name ? this.name : this.constructor.name.toLowerCase();
    }

    /**
     * Executed on ServiceProvider boot.
     */
    mounted() {}

    /**
     * Trigger an event.
     * @param {string} eventName
     */
    #trigger(eventName) {
        const events = this.app.get('events');

        events.dispatch(eventName, this);
        events.dispatch(`${this.constructor.name}.${eventName}`, this);
    }
}
