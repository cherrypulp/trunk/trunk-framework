import Container from './Container';

export default class Dispatcher {
    /**
     * The IoC container instance.
     * @type {Container|null}
     */
    #container = null;

    /**
     * The registered event listeners.
     * @type {Map<String, Array>}
     */
    #listeners = new Map();

    /**
     * The wildcard listeners.
     * @type {Map<String, Array>}
     */
    #wildcards = new Map();

    /**
     * The cached wildcard listeners.
     * @type {Map<String, Array>}
     */
    #wildcardsCache = new Map();

    /**
     * @param container
     */
    constructor(container) {
        this.#container = container ?? new Container();
    }

    /**
     * Fire an event and call the listeners.
     * @param event
     * @param payload
     * @param halt
     * @return {*}
     */
    dispatch(event, payload = {}, halt = false) {
        const listeners = this.getListeners(event);
        const responses = [];
        let continues = true;

        listeners.forEach((listener) => {
            if (continues) {
                const response = listener(event, payload);

                if (halt && response !== null) {
                    return response;
                }

                if (response === false) {
                    continues = false;
                } else {
                    responses.push(response);
                }
            }
        });

        return halt ? null : responses;
    }

    /**
     * Fire an event and call the listeners.
     * @param {string} event
     * @param payload
     * @param {boolean} halt
     * @return {*}
     */
    fire(event, payload = {}, halt = false) {
        return this.dispatch(event, payload, halt);
    }

    /**
     * Flush a set of pushed events.
     * @param {string} event
     */
    flush(event) {
        this.dispatch(`${event}_pushed`);
    }

    /**
     * Remove a set of listeners from the dispatcher.
     * @param {string} event
     */
    forget(event) {
        if (event.includes('*')) {
            this.#wildcards.delete(event);
        } else {
            this.#listeners.delete(event);
        }
    }

    /**
     * Forget all of the pushed listeners.
     */
    forgetPushed() {
        this.#listeners.forEach((listener, key) => {
            if (key.endsWith('_pushed')) {
                this.forget(key);
            }
        });
    }

    /**
     * Get all of the listeners for a given event name.
     * @param {string} eventName
     * @return {*[]}
     */
    getListeners(eventName) {
        const listeners = this.#listeners.get(eventName) || [];
        const wildcards = this.#wildcardsCache.has(eventName)
            ? this.#wildcardsCache.get(eventName)
            : this.#getWildcardListeners(eventName);

        return [].concat(listeners, wildcards);
    }

    /**
     * Get the wildcard listeners for the event.
     * @param {string} eventName
     * @return {{}}
     */
    #getWildcardListeners(eventName) {
        let wildcards = [];

        this.#wildcards.forEach((listeners, key) => {
            if (key === eventName) {
                wildcards = [].concat(wildcards, listeners);
            }
        });

        this.#wildcardsCache.set(eventName, wildcards);

        return wildcards;
    }

    /**
     * Determine if a given event has listeners.
     * @param {string} eventName
     * @return {Boolean}
     */
    hasListeners(eventName) {
        const listeners = this.#listeners.get(eventName) || [];
        const wildcards = this.#wildcards.get(eventName) || [];
        return listeners.length > 0 || wildcards.length > 0;
    }

    /**
     * Register an event listener with the dispatcher.
     * @param {String|Array} events
     * @param {Function} listener
     */
    listen(events, listener) {
        if (!Array.isArray(events)) {
            events = events.split(' ');
        }

        events.forEach((event) => {
            if (event.includes('*')) {
                this.#setupWildcardListener(event, listener);
            } else {
                const listeners = this.#listeners.get(event) || [];
                listeners.push(this.makeListener(listener));
                this.#listeners.set(event, listeners);
            }
        });
    }

    /**
     * Register an event listener with the dispatcher.
     * @param {Function} listener
     * @param {boolean} wildcard
     * @return {function(...[*]=)}
     */
    makeListener(listener, wildcard = false) {
        return (event, payload) => {
            if (wildcard) {
                return listener(event, payload);
            }

            return listener(payload);
        };
    }

    /**
     * Register an event and payload to be fired later.
     * @param {string} event
     * @param payload
     */
    push(event, payload = {}) {
        this.listen(`${event}_pushed`, () => {
            this.dispatch(event, payload);
        });
    }

    /**
     * Resolve the subscriber instance.
     * @param {Object|String} subscriber
     * @return {*}
     */
    #resolveSubscriber(subscriber) {
        if (typeof subscriber === 'string') {
            return this.#container.make(subscriber);
        }

        return subscriber;
    }

    /**
     * Setup a wildcard listener callback.
     * @param {string} event
     * @param listener
     */
    #setupWildcardListener(event, listener) {
        const wildcards = this.#wildcards.get(event) || [];
        wildcards.push(this.makeListener(listener, true));
        this.#wildcards.set(event, wildcards);
        this.#wildcardsCache = new Map();
    }

    /**
     * Register an event subscriber with the dispatcher.
     * @param {Dispatcher|Object|String} subscriber
     */
    subscribe(subscriber) {
        subscriber = this.#resolveSubscriber(subscriber);
        subscriber.subscribe(this);
    }

    /**
     * Fire an event until the first non-null response is returned.
     * @param {string} event
     * @param payload
     * @return {*}
     */
    until(event, payload = {}) {
        return this.dispatch(event, payload, true);
    }
}
