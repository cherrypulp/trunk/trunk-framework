/**
 * Extended Error object with the option to set error `status` and `code`.
 *
 * @example
 * ```js
 * new Exception('message', 500, 'E_RUNTIME_EXCEPTION');
 * ```
 */
export default class Exception extends Error {
    /**
     * Exception code.
     * @type {String|null}
     */
    code = null;

    /**
     * Exception message.
     * @type {String}
     */
    message = 'Exception';

    /**
     * Exception name.
     * @type {String}
     */
    name = null;

    /**
     * Exception status number.
     * @type {Number}
     */
    status = null;

    /**
     * Exception constructor.
     * @param message
     * @param status
     * @param code
     */
    constructor(message, status = 500, code = null) {
        super(message);

        // Set error message
        Object.defineProperty(this, 'message', {
            configurable: true,
            enumerable: false,
            value: code ? `${code}: ${message}` : message,
            writable: true,
        });

        // Set error name as public property
        Object.defineProperty(this, 'name', {
            configurable: true,
            enumerable: false,
            value: this.constructor.name,
            writable: true,
        });

        // Set status as a public property
        if (code) {
            Object.defineProperty(this, 'code', {
                configurable: true,
                enumerable: false,
                value: code,
                writable: true,
            });
        }

        // Update stack trace
        Error.captureStackTrace(this, this.constructor);
    }
}
