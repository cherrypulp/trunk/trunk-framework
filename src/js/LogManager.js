import Exception from './Exception';

export default class LogManager {
    /**
     * Active channel.
     * @type {[]}
     */
    #activeChannels = [];

    /**
     * The IoC container instance.
     * @type {Container|null}
     */
    #container = null;

    /**
     * The array of resolved channels.
     * @type {{}}
     */
    #channels = {};

    /**
     * The event dispatcher instance.
     * @type {Dispatcher|null}
     */
    #dispatcher = null;

    /**
     * Log levels.
     * @type {}
     */
    #levels = {
        emergency: 600,
        alert: 550,
        critical: 500,
        error: 400,
        warning: 300,
        notice: 250,
        info: 200,
        debug: 100,
        log: 50,
        off: 0,
    };

    /**
     * Log an alert message to the logs.
     * @param message
     * @param context
     */
    alert(message, ...context) {
        this.#writeLog('alert', message, context);
    }

    /**
     * Get a log channel instance.
     * @example
     *     this.channel().log(something);
     * @example
     *     this.channel('console').log(something);
     * @example
     *     this.channel(['console', 'report']).log(something);
     * @param name
     * @return {*}
     */
    channel(name = 'console') {
        if (!Array.isArray(name)) {
            name = [name];
        }

        this.#activeChannels = name;
        return this;
    }

    /**
     * Create a new Log manager instance.
     * @param container
     */
    constructor(container) {
        this.#container = container;

        if (!this.#container.has('events')) {
            throw new Exception(
                'Events dispatcher has not been set.',
                500,
                'E_RUNTIME_EXCEPTION',
            );
        }

        this.#dispatcher = this.#container.get('events');
    }

    /**
     * Log a critical message to the logs.
     * @param message
     * @param context
     */
    critical(message, ...context) {
        this.#writeLog('critical', message, context);
    }

    /**
     * Log a debug message to the logs.
     * @param message
     * @param context
     */
    debug(message, ...context) {
        this.#writeLog('debug', message, context);
    }

    /**
     * Log an emergency message to the logs.
     * @param message
     * @param context
     */
    emergency(message, ...context) {
        this.#writeLog('emergency', message, context);
    }

    /**
     * Log an error message to the logs.
     * @param message
     * @param context
     */
    error(message, ...context) {
        this.#writeLog('error', message, context);
    }

    /**
     * Fire a log event.
     * @param level
     * @param message
     * @param context
     */
    #fireLogEvent(level, message, context = []) {
        if (this.#dispatcher) {
            this.#dispatcher.dispatch(level, {
                level,
                message,
                context,
            });
        }
    }

    /**
     * Log an informational message to the logs.
     * @param message
     * @param context
     */
    info(message, ...context) {
        this.#writeLog('info', message, context);
    }

    /**
     * Register a new callback handler for when a log event is triggered.
     * @param level
     * @param listener
     */
    listen(level, listener) {
        this.#dispatcher.listen(level, listener);
    }

    /**
     * Log a message to the logs.
     * @param message
     * @param context
     */
    log(message, ...context) {
        this.#writeLog('log', message, context);
    }

    /**
     * Log a notice to the logs.
     * @param message
     * @param context
     */
    notice(message, ...context) {
        this.#writeLog('notice', message, context);
    }

    /**
     * Resolve the given log instance by name.
     * @param name
     * @return {*}
     */
    #resolve(name) {
        if (this.#channels[name] === undefined) {
            const config = this.#container
                .get('config')
                .getThrough(`logging.channels.${name}`);

            if (config === null) {
                throw new Exception(
                    `Log [${name}] is not defined.`,
                    500,
                    'E_INVALID_ARGUMENT',
                );
            }

            this.#channels[name] = {
                bubble: config.bubble ?? true,
                level: config.level,
                logger: new config.logger(this.#container, {
                    ...config,
                    logger: undefined,
                }),
            };
        }

        return this.#channels[name];
    }

    /**
     * Log a warning message to the logs.
     * @param message
     * @param context
     */
    warning(message, ...context) {
        this.#writeLog('warning', message, context);
    }

    /**
     * Write a message to the log.
     * @param level
     * @param message
     * @param context
     */
    #writeLog(level, message, context = []) {
        this.#fireLogEvent(level, message, context);

        let continues = true;

        this.#activeChannels.forEach((name) => {
            if (!continues) {
                return;
            }

            const channel = this.#resolve(name);

            if (this.#levels[channel.level] >= this.#levels[level]) {
                channel.logger[level](message, context);
                continues = channel.bubble;
            }
        });

        // reset active channels
        this.#activeChannels = Object.keys(this.#channels);
    }
}
