import Exception from './Exception';

export default class Logger {
    /**
     * The IoC container instance.
     * @type {Container|null}
     */
    #container = null;

    /**
     * Logger options.
     * @type {object}
     */
    #options = {};

    /**
     * Log an alert message to the logs.
     * @param message
     * @param context
     */
    alert(message, context = []) {}

    /**
     * Create a new Logger instance.
     * @param {Application|Container} container
     * @param {object} options
     */
    constructor(container, options = {}) {
        this.#container = container;
        this.#options = options;
    }

    /**
     * Log a critical message to the logs.
     * @param message
     * @param context
     */
    critical(message, context = []) {}

    /**
     * Log a debug message to the logs.
     * @param message
     * @param context
     */
    debug(message, context = []) {}

    /**
     * Log an emergency message to the logs.
     * @param message
     * @param context
     */
    emergency(message, context = []) {}

    /**
     * Log an error message to the logs.
     * @param message
     * @param context
     */
    error(message, context = []) {}

    /**
     * Log an informational message to the logs.
     * @param message
     * @param context
     */
    info(message, context = []) {}

    /**
     * Log a message to the logs.
     * @param message
     * @param context
     */
    log(message, context = []) {}

    /**
     * Log a notice to the logs.
     * @param message
     * @param context
     */
    notice(message, context = []) {}

    /**
     * Log a warning message to the logs.
     * @param message
     * @param context
     */
    warning(message, context = []) {}
}
