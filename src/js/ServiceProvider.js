export default class ServiceProvider {
    /**
     * Application container.
     * @type {Application|null}
     */
    app = null;

    /**
     * Additional bindings.
     * @type {{}}
     */
    bindings = {};

    /**
     * Additional singletons.
     * @type {{}}
     */
    singletons = {};

    /**
     * Additional providers to load.
     * @type {Array}
     */
    providers = [];

    /**
     * ServiceProvider constructor.
     * @param {Application|Container} container
     */
    constructor(container) {
        this.app = container;
    }

    /**
     * Boot.
     */
    boot() {}

    /**
     * Register.
     */
    register() {}

    /**
     * Register provider and dependency providers.
     */
    registerProviders() {
        this.providers.forEach((provider) => {
            this.app.register(provider);
        });

        this.register();
    }
}
