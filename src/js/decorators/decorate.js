import {
    decorate as _decorate,
    createDefaultSetter,
} from '../helpers/decorate';

const { defineProperty } = Object;

function handleDescriptor(target, key, descriptor, [decorator, ...args]) {
    const { configurable, enumerable, writable } = descriptor;
    const originalGet = descriptor.get;
    const originalSet = descriptor.set;
    const originalValue = descriptor.value;
    const isGetter = !!originalGet;

    return {
        configurable,
        enumerable,
        get() {
            const fn = isGetter ? originalGet.call(this) : originalValue;
            const value = decorator.call(this, fn, ...args);

            if (isGetter) {
                return value;
            }

            const desc = {
                configurable,
                enumerable,
            };

            desc.value = value;
            desc.writable = writable;

            defineProperty(this, key, desc);

            return value;
        },
        set: isGetter ? originalSet : createDefaultSetter(),
    };
}

/**
 * Immediately applies the provided function and arguments to the method.
 * The first argument is the function to apply, all further arguments will be passed to that decorating function.
 * @example
 * class Foo {
 *     \@decorate(memoize)
 *     foo() {}
 * }
 * @param args
 * @return {function(): *}
 */
export default function decorate(...args) {
    return _decorate(handleDescriptor, args);
}
