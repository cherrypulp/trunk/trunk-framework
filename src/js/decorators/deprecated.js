import { decorate } from '../helpers/decorate';

const DEFAULT_MSG = 'This function will be removed in future versions.';

function handleDescriptor(
    target,
    key,
    descriptor,
    [msg = DEFAULT_MSG, options = {}],
) {
    if (typeof descriptor.value !== 'function') {
        throw new SyntaxError('Only functions can be marked as deprecated');
    }

    const methodSignature = `${target.constructor.name}#${key}`;

    if (options.url) {
        msg += `\n\n    See ${options.url} for more details.\n\n`;
    }

    return {
        ...descriptor,
        value: function deprecationWrapper() {
            console.warn(
                '%cDEPRECATION%c %s',
                'background-color: #e88388; color: #fff;',
                '',
                `${methodSignature}: ${msg}`,
            );
            return descriptor.value.apply(this, arguments);
        },
    };
}

/**
 * Calls console.warn() with a deprecation message. Provide a custom message to override the default one.
 * You can also provide an options hash with a url, for further reading.
 * @param args
 * @return {function(): *}
 */
export default function deprecated(...args) {
    return decorate(handleDescriptor, args);
}
