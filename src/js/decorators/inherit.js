/**
 * Inherit from multiple classes.
 * @param args
 * @return {function(*=): *}
 */
export function inherit(...args) {
    const classes = args.reverse();

    return function(target) {
        classes.forEach((klass) => {
            const keys = Object.getOwnPropertyNames(klass.prototype);

            keys.forEach((key) => {
                if (
                    !target.prototype[key] &&
                    typeof klass.prototype[key] === 'function'
                ) {
                    target.prototype[key] = klass.prototype[key];
                }
            });
        });

        return target;
    };
}
