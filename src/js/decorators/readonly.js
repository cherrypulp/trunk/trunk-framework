import { decorate } from '../helpers/decorate';

function handleDescriptor(target, key, descriptor) {
    descriptor.writable = false;
    return descriptor;
}

/**
 * Marks a property or method as not being writable.
 * @param args
 * @return {function(): *}
 */
export default function readonly(...args) {
    return decorate(handleDescriptor, args);
}
