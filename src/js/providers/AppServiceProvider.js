import ServiceProvider from '../ServiceProvider';
import EventServiceProvider from './EventServiceProvider';
import ConfigServiceProvider from './ConfigServiceProvider';
import LogServiceProvider from './LogServiceProvider';

export default class AppServiceProvider extends ServiceProvider {
    /**
     * Additional providers.
     * @type {Array}
     */
    providers = [
        new EventServiceProvider(this.app),
        new ConfigServiceProvider(this.app),
        new LogServiceProvider(this.app),
    ];

    /**
     * Register.
     */
    register() {
        const config = this.app.get('config');

        // expose the app to a global
        if (config.hasThrough('app.name')) {
            window[config.getThrough('app.name')] = this.app;
        }

        // register providers from config
        config.getThrough('app.providers', []).forEach((provider) => {
            this.app.register(provider);
        });
    }
}
