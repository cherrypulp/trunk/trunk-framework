import ServiceProvider from '../ServiceProvider';
import Collection from '../Collection';
import * as baseConfigs from '../config/';
import { mapRequiredFiles } from '../helpers/require';

export default class ConfigServiceProvider extends ServiceProvider {
    /**
     * Register.
     */
    register() {
        // add dependency to the container
        this.app.singleton('config', () => {
            return new Collection({});
        });

        try {
            const config = this.app.get('config');

            // load default configurations
            Object.entries(baseConfigs).forEach(([key, value]) => {
                config.put(key.toLowerCase(), value);
            });

            // load application configurations
            // @note - webpack need require here to link with folder name
            const appConfigs = mapRequiredFiles(
                require.context('@/js', true, /config\/.*\.js$/),
            );
            Object.entries(appConfigs).forEach(([key, value]) => {
                key = key.toLowerCase();
                // @TODO - merge recursively
                config.put(key, {
                    ...config.get(key),
                    ...value,
                });
            });
        } catch (e) {
            // console.error(e);
            throw e;
        }
    }
}
