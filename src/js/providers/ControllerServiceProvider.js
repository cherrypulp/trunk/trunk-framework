import Dispatcher from '../Dispatcher';
import ServiceProvider from '../ServiceProvider';
import { mapRequiredFiles } from '../helpers/require';

export default class ControllerServiceProvider extends ServiceProvider {
    /**
     * Controllers.
     */
    #controllers = {
        // @note - ensure DefaultController is the first one booted
        DefaultController: null,
    };

    #fireController(instance, action = 'doCreated') {
        if (instance.isActive()) {
            instance[action]();
        }
    }

    /**
     * Get all controllers.
     * @return {{[p: string]: *}}
     */
    #getControllers() {
        // @note - webpack need require here to link with folder name
        const controllers = mapRequiredFiles(
            require.context('@/js', true, /controllers\/.*\.js$/),
        );

        return { ...this.#controllers, ...controllers };
    }

    /**
     * Register.
     */
    register() {
        // load application controllers
        this.#controllers = this.#getControllers();

        // Register controllers then create if needed.
        Object.entries(this.#controllers).forEach(([name, controller]) => {
            this.app.singleton(name, () => {
                /* eslint-disable-next-line */
                return new controller(this.app);
            });

            this.app.with(name, (instance) =>
                this.#fireController(instance, 'doCreated'),
            );
        });
    }

    /**
     * Boot.
     */
    boot() {
        Object.keys(this.#controllers).forEach((name) => {
            this.app.with(name, (instance) =>
                this.#fireController(instance, 'doMounted'),
            );
        });
    }
}
