import ServiceProvider from '../ServiceProvider';
import Dispatcher from '../Dispatcher';

export default class EventServiceProvider extends ServiceProvider {
    /**
     * Register events.
     */
    register() {
        this.app.singleton('events', () => {
            return new Dispatcher(this.app);
        });
    }
}
