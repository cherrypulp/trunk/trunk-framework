import ServiceProvider from '../ServiceProvider';
import LogManager from '../LogManager';
import EventServiceProvider from './EventServiceProvider';
import Exception from '../Exception';

export default class LogServiceProvider extends ServiceProvider {
    providers = [new EventServiceProvider(this.app)];

    /**
     * Register Logger.
     */
    register() {
        if (window !== undefined) {
            // catch uncaught errors
            window.onerror = function (
                errorMsg,
                url,
                lineNumber,
                colNumber,
                error,
            ) {
                if (error instanceof Exception || error instanceof Error) {
                    this.app
                        .get('log')
                        .critical(
                            `[${error.name}] ${error.message} with code ${error.code} at line ${lineNumber}:${colNumber} in ${url}\n`,
                            error.stack,
                        );
                } else {
                    this.app
                        .get('log')
                        .critical(
                            `${errorMsg} at line ${lineNumber}:${colNumber} in ${url} (${error.stack})`,
                        );
                }

                return true;
            };
        }

        this.app.singleton('log', (app) => {
            return new LogManager(app);
        });
    }
}
