import Vue from 'vue';
import ServiceProvider from '../ServiceProvider';

export default class VueServiceProvider extends ServiceProvider {
    register() {
        /**
         * @example
         * this.app.make('Vue', {
         *     el: '.app',
         * });
         */
        this.app.bind('Vue', (app, options = null) => {
            if (options) {
                return new Vue(options);
            }

            return Vue;
        });
    }

    boot() {
        const config = {
            el: null,
            components: {},
            directives: {},
            mixins: [],
            options: {},
            plugins: [],
            ...this.app.get('config').get('vue'),
        };

        this.app.get('Vue').config.errorHandler = (error, vue, info) => {
            if (error instanceof Error) {
                this.app.get('log').critical(
                    `${error.name} thrown with "${error.message}" at ${info}\n`,
                    error.stack,
                    '\n',
                    vue,
                );
            } else {
                this.app.get('log').critical(`"${error}" at ${info}\n`, '\n', vue);
            }
        };

        config.plugins.forEach((value) => {
            if (Array.isArray(value)) {
                this.app.get('Vue').use(value[0], value[1] ?? null);
            } else {
                this.app.get('Vue').use(value);
            }
        });

        config.mixins.forEach((value) => {
            this.app.get('Vue').mixin(value);
        });

        Object.entries(config.components).forEach(([key, value]) => {
            this.app.get('Vue').component(key, value);
        });

        Object.entries(config.directives).forEach(([key, value]) => {
            this.app.get('Vue').directive(key, value);
        });

        if (config.el) {
            this.app.make('Vue', {
                el: config.el,
                ...config.options,
            });
        }
    }
}
