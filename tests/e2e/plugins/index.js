// https://docs.cypress.io/guides/guides/plugins-guide.html
const path = require('path');
require('dotenv').config({
    path: path.resolve(`${__dirname}/../../../../../../.env`),
});

// if you need a custom webpack configuration you can uncomment the following import
// and then use the `file:preprocessor` event
// as explained in the cypress docs
// https://docs.cypress.io/api/plugins/preprocessors-api.html#Examples

/* eslint-disable import/no-extraneous-dependencies, global-require, arrow-body-style */
// const webpack = require('@cypress/webpack-preprocessor')

module.exports = (on, config) => {
    // on('file:preprocessor', webpack({
    //  webpackOptions: require('@vue/cli-service/webpack.config'),
    //  watchOptions: {}
    // }))

    config.baseUrl = process.env.WP_SITEURL;
    config.env = {
        siteurl: process.env.WP_SITEURL,
        title: process.env.WP_TITLE,
    };

    return Object.assign({}, config, {
        fixturesFolder: 'tests/e2e/fixtures',
        integrationFolder: 'tests/e2e/specs',
        screenshotsFolder: 'tests/e2e/screenshots',
        videosFolder: 'tests/e2e/videos',
        supportFile: 'tests/e2e/support/index.js',
    });
};
// module.exports = (on, config) => {
//     config = dotenvPlugin(config)
//     return config
// }
