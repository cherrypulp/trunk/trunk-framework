// https://docs.cypress.io/api/introduction/api.html
const config = Cypress.config();
const env = Cypress.env();

describe('Testing home', () => {
    beforeEach(() => {
        console.info(`[e2e] -> ${config.spec.relative}`, env);
    });

    it('Visits the app root url', () => {
        cy.visit('/');
        cy.contains('h1', 'Latest Posts');
    });

    it('A user can switch languages', () => {
        // @TODO
        expect(false).to.be.true;
    });

    it('Visits an article', () => {
        cy.visit('/');
        cy.get('.entry-title a').click();
        cy.contains('h1', 'Hello world!');
    });
});
