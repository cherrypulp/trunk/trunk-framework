import { expect } from 'chai';
import Application from '../../src/js/Application';
import TestServiceProvider from './TestServiceProvider';

describe('Application.js', () => {
    describe('new Application()', () => {
        const app = new Application();

        it('should return a new Application instance', () => {
            expect(app.constructor.name).to.be.equal('Application');
        });

        it('should already have Container bind', () => {
            expect(app.get('Container').constructor.name).to.be.equal(
                'Application',
            );
        });

        it('should already have EventServiceProvider registered', () => {
            expect(app.getProvider('EventServiceProvider')).to.not.be.null;
        });

        it('should already have ConfigServiceProvider registered', () => {
            expect(app.getProvider('ConfigServiceProvider')).to.not.be.null;
        });

        it('should then have "config" bind', () => {
            expect(app.get('config').constructor.name).to.be.equal(
                'Collection',
            );
        });

        it('should already have LogServiceProvider registered', () => {
            expect(app.getProvider('LogServiceProvider')).to.not.be.null;
        });

        it('should already have AppServiceProvider registered', () => {
            expect(app.getProvider('AppServiceProvider')).to.not.be.null;
        });

        it('should have create a global called "trunk"', () => {
            expect(window.trunk.constructor.name).to.be.equal('Application');
        });
    });

    describe('boot()', () => {
        const app = new Application();
        app.register(new TestServiceProvider(app));

        it('should have boot the application services provider', () => {
            app.boot();
            expect(app.get('checkIfBooted')).to.be.equal('booted');
        });
    });

    describe('getProvider()', () => {
        const app = new Application();
        app.register(new TestServiceProvider(app));

        it('should return a provider from the given name', () => {
            expect(
                app.getProvider('TestServiceProvider').constructor.name,
            ).to.be.equal('TestServiceProvider');
        });

        it('should return null if the given name is not a registered provider', () => {
            expect(app.getProvider('NoopServiceProvider')).to.be.equal(null);
        });
    });

    describe('register()', () => {
        const app = new Application();

        it('should return the given service provider', () => {
            const provider = app.register(new TestServiceProvider(app));
            expect(provider.constructor.name).to.be.equal(
                'TestServiceProvider',
            );
        });

        it('should register a service provider', () => {
            expect(
                app.getProvider('TestServiceProvider').constructor.name,
            ).to.be.equal('TestServiceProvider');
        });

        it('should resolve and register a service provider', () => {
            expect(
                app.resolveProvider('TestServiceProvider').constructor.name,
            ).to.be.equal('TestServiceProvider');
        });
    });

    describe('shortcut()', () => {
        const app = new Application();

        it('should add a shortcut for the given dependency', () => {
            app.bind('Foo', () => 'foo');
            app.shortcut('Foo');
            /*eslint-disable new-cap*/
            expect(app.Foo).to.be.equal('foo');
        });

        it('should add a shortcut for the given dependency with the given custom name', () => {
            app.bind('Foo', () => 'foo');
            app.shortcut('Foo', 'foo');
            expect(app.foo).to.be.equal('foo');
        });

        it('should add a shortcut for the given dependency with the given descriptor', () => {
            app.bind('Foo', () => 'foo');
            app.shortcut('Foo', 'bar', {
                value: 'baz',
            });
            expect(app.bar).to.be.equal('baz');
        });
    });

    describe('Application.getInstance()', () => {
        const app = new Application();

        it('should return the instance of the singleton', () => {
            app.bind('name', 'My app');
            expect(Application.getInstance().get('name')).to.be.equal(
                app.get('name'),
            );
        });
    });

    describe('Application.setInstance()', () => {
        it('should set the given instance to the singleton instance');
    });
});
