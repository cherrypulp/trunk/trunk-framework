import { expect } from 'chai';
import Collection from '../../src/js/Collection';

describe('Collection.js', () => {
    const collection = new Collection({
        foo: {
            bar: 'baz',
        },
    });

    describe('getThrough()', () => {
        it('should get a nested value from given path', () => {
            expect(collection.getThrough('foo.bar')).to.equal('baz');
            expect(collection.getThrough('aze.brah')).to.be.null;
        });

        it('should return a default value if a nested value is not found', () => {
            expect(collection.getThrough('foo.bar.brah', 'nok')).to.equal(
                'nok',
            );
        });
    });

    describe('hasThrough()', () => {
        it('should return true if key exists', () => {
            expect(collection.hasThrough('foo')).to.be.true;
            expect(collection.hasThrough('foo.bar')).to.be.true;
            expect(collection.hasThrough(['foo.bar', 'foo'])).to.be.true;
        });

        it('should return false if key not exists', () => {
            expect(collection.hasThrough('bar')).to.be.false;
            expect(collection.hasThrough('foo.frah')).to.be.false;
            expect(collection.hasThrough(['foo', 'frah'])).to.be.false;
        });
    });

    describe('setThrough()', () => {
        it('should set a nested value from given path', () => {
            collection.setThrough('foo.bar', 'ok');
            collection.setThrough('another.something', 'ok');
            expect(collection.all()).to.eql({
                foo: {
                    bar: 'ok',
                },
                another: {
                    something: 'ok',
                },
            });
        });
    });
});
