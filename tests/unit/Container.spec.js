import { expect } from 'chai';
import Container from '../../src/js/Container';

describe('Container.js', () => {
    let container;

    beforeEach(() => {
        container = new Container();
    });

    describe('new Container()', () => {
        it('should create an instance of Container', () => {
            expect(container.constructor.name).to.be.equal('Container');
        });
    });

    describe('alias()', () => {
        it('should add an alias for a dependency', () => {
            container.bind('Foo', () => 'foo');
            container.alias('Foo', 'bar');
            expect(container.has('bar', true)).to.be.true;
            expect(container.get('bar')).to.be.equal('foo');
        });
    });

    describe('bind()', () => {
        it('should bind a dependency', () => {
            container.bind('Foo', () => 'foo');
            expect(container.has('Foo')).to.be.true;
        });
    });

    describe('get()', () => {
        it('should get a dependency', () => {
            container.bind('Foo', () => 'foo');
            expect(container.get('Foo')).to.be.equal('foo');
        });
    });

    describe('getAlias()', () => {
        it('should get a dependency from alias name', () => {
            container.bind('Foo', () => 'foo');
            container.alias('Foo', 'bar');
            expect(container.getAlias('bar')).to.be.equal('Foo');
        });
    });

    describe('has()', () => {
        it('should return true if a dependency exists with the given name', () => {
            container.bind('Foo', () => 'foo');
            expect(container.has('Foo')).to.be.true;
        });

        it('should return false if a dependency does not exists with the given name', () => {
            expect(container.has('Baz')).to.be.false;
        });
    });

    describe('instance()', () => {
        it('should test this method');
    });

    describe('make()', () => {
        it('should test this method');
    });

    describe('resolve()', () => {
        it('should test this method');
    });

    describe('singleton()', () => {
        it('should test this method');
    });

    describe('use()', () => {
        it('should test this method');
    });

    describe('with()', () => {
        it('should test this method');
    });
});
