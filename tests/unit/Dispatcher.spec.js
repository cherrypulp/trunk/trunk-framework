import { expect } from 'chai';
import Dispatcher from '../../src/js/Dispatcher';

describe('Dispatcher.js', () => {
    describe('new Dispatcher()', () => {
        it('should create an instance of Dispatcher', () => {
            const events = new Dispatcher();
            expect(events.constructor.name).to.be.equal('Dispatcher');
        });
    });

    describe('dispatch()', () => {
        it('should dispatch an event', (done) => {
            const events = new Dispatcher();
            events.listen('foo', () => {
                expect(true).to.be.true;
                done();
            });
            events.dispatch('foo');
        });

        it('should dispatch an event with payload', (done) => {
            const events = new Dispatcher();
            events.listen('foo', (payload) => {
                expect(payload).to.be.equal('ok');
                done();
            });
            events.dispatch('foo', 'ok');
        });

        it('should dispatch a wildcard event', (done) => {
            const events = new Dispatcher();
            events.listen('foo.*', (event, payload) => {
                expect(payload).to.be.equal('ok');
                done();
            });
            events.dispatch('foo.*', 'ok');
        });
    });
});
