import { expect } from 'chai';
import LogManager from '../../src/js/LogManager';
import Container from '../../src/js/Container';
import Dispatcher from '../../src/js/Dispatcher';
import Collection from '../../src/js/Collection';
import ConsoleLogger from '../../src/js/loggers/ConsoleLogger';

describe('LogManager.js', () => {
    let manager = null;

    before(() => {
        const container = new Container();
        container.singleton('events', () => {
            return new Dispatcher(container);
        });
        container.singleton('config', () => {
            return new Collection({
                logging: {
                    channels: {
                        console: {
                            level: 'debug',
                            logger: ConsoleLogger,
                        },
                    },
                },
            });
        });
        manager = new LogManager(container);
    });

    // describe('new LogManager()', () => {
    //
    // });

    // describe('channel()', () => {
    //     it('does something', () => {
    //         manager.channel().log('okkk');
    //     });
    // });
});
