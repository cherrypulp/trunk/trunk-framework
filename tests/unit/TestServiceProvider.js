import ServiceProvider from '../../src/js/ServiceProvider';

export default class TestServiceProvider extends ServiceProvider {
    register() {
        this.app.bind('checkIfRegistered', 'registered');
    }

    boot() {
        this.app.bind('checkIfBooted', 'booted');
    }
}
