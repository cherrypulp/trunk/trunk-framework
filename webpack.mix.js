const path = require('path');
const mix = require('laravel-mix');
require('laravel-mix-stylelint');
require('laravel-mix-eslint');

const paths = {
    dist: 'dist',
    src: 'src',
};

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(paths.dist)

    .eslint({
        cache: process.env.NODE_ENV === 'production',
    })
    .js(`${paths.src}/index.js`, 'js')

    .stylelint({
        context: './',
        files: ['**/*.scss', '**/*.vue'],
        syntax: null,
    })
    .sass(`${paths.src}/index.scss`, 'css')

    .sourceMaps()
    .version();

const options = {
    postCss: [
        // @see https://github.com/ismamz/postcss-utilities
        require('postcss-utilities'),
        // @see https://github.com/cssnano/cssnano
        require('cssnano'),
    ],
    processCssUrls: false,
};
const webpackOptions = {
    context: path.resolve(__dirname, paths.src),
    devtool: 'inline-source-map',
    module: {
        // @see https://www.npmjs.com/package/import-glob-loader
        rules: [
            {
                test: /\.sass$/,
                use: [
                    'webpack-import-glob-loader',
                    'css-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    // Reads Sass vars from files or inlined in the options property
                    {
                        loader: '@epegzz/sass-vars-loader',
                        options: {
                            syntax: 'scss',
                            // Option 1) Specify vars here
                            vars: {
                                debug: process.env.NODE_ENV === 'development',
                            },
                        },
                    },
                    'webpack-import-glob-loader',
                ],
            },
            {
                test: /\.js$/,
                use: ['webpack-import-glob-loader'],
            },
        ],
    },
    resolve: {
        alias: {
            '@': path.resolve(__dirname, paths.src),
        },
    },
    target: 'web',
    plugins: [
        // @note - Bundle analyzer
        //new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)(),
    ],
};

mix.options(options).webpackConfig(webpackOptions);
